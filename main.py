import copy
import io
import tempfile
import openpyxl
import pandas as pd
import tqdm
from docx import Document
import os
if not os.path.exists('output/teacher_yellow_report'):
    os.makedirs('output/teacher_yellow_report')
if not os.path.exists('output/access_yellow_report'):
    os.makedirs('output/access_yellow_report')
if not os.path.exists('output/community_partners'):
    os.makedirs('output/community_partners')
if not os.path.exists('output/counselor_report'):
    os.makedirs('output/counselor_report')

EXCEL_FILE = 'Raider Report Query_Week09_11-12-19.xlsx'
SHEET = 'For Mail Merge'
SEMESTER = 1
QUARTER = 1
PERIODS = (1, 2, 3, 4, 5, 6, 7)
DO_COMMUNITY = True
DO_ACCESS = True
DO_TEACHER = True
DO_COUSELING = True

big_loop = tqdm.tqdm(total=7, desc="Generating individual reports:")

big_loop.set_description("Loading excel file.")
full_data = pd.read_excel(EXCEL_FILE, sheet_name=SHEET, header=1)
#workbook = openpyxl.load_workbook(EXCEL_FILE)
big_loop.update()

#big_loop.set_description("Generate access teacher list")
#access_names = set(full_data['Access / AVID Teacher'].unique())

#access_names = set(full_data['RR Teacher'].dropna().unique())
#access_names.discard(float('nan'))
#big_loop.update()

big_loop.set_description("Generating teacher and access list")

### Not as friendly loop to build list of teachers and access teachers. Goes through the WHOLE thing!
teacher_names = set([])
access_names = set([])
counselor_names = set([])
for index, row in full_data.iterrows():
    for period in PERIODS:
        if type(row['P' + str(period) + ' S' + str(SEMESTER) + ' Teacher']) is str:
            teacher_names.add(row['P' + str(period) + ' S' + str(SEMESTER) + ' Teacher'])
            if type(row['P' + str(period) + ' S' + str(SEMESTER) + ' Course']) is str and '9th Grade Access' in row['P' + str(period) + ' S' + str(SEMESTER) + ' Course']:
                access_names.add(row['P' + str(period) + ' S' + str(SEMESTER) + ' Teacher'])
            if type(row['P' + str(period) + ' S' + str(SEMESTER) + ' Course']) is str and 'AVID' in row['P' + str(period) + ' S' + str(SEMESTER) + ' Course']:
                access_names.add(row['P' + str(period) + ' S' + str(SEMESTER) + ' Teacher'])
            if type(row['P' + str(period) + ' S' + str(SEMESTER) + ' Course']) is str and 'Academic Skills Tutorial' in row['P' + str(period) + ' S' + str(SEMESTER) + ' Course']:
                access_names.add(row['P' + str(period) + ' S' + str(SEMESTER) + ' Teacher'])
            if type(row['P' + str(period) + ' S' + str(SEMESTER) + ' Course']) is str and 'ELD Access' in row['P' + str(period) + ' S' + str(SEMESTER) + ' Course']:
                access_names.add(row['P' + str(period) + ' S' + str(SEMESTER) + ' Teacher'])
    #if type(row['Counselor']) is str:
    #    counselor_names.add(row['Counselor'])
big_loop.update()

counselor_names = set(full_data['Counselor'].dropna().unique())
#print("Counsekir:", counselor_names)

big_loop.set_description("Generating Community Partners")
community_partners = set(full_data['Community Partner Organization'].dropna().unique())
big_loop.update()

def is_access(proposed_name: str) -> bool:
    '''This function is needed because RR Teacher is Last, First M., while course listings are Last, First with no middle initial.
    '''
    if type(proposed_name) is not str:
        return False
    for teacher in access_names:
        if proposed_name.startswith(teacher) or teacher.startswith(proposed_name):
            return True
    return False


def is_name(name1: str, name2: str) -> bool:
    '''
    Returns True if the names are the same, mostly a check for middle initials.
    '''
    if type(name1) is not str or type(name2) is not str:
        return False
    return name1.startswith(name2) or name2.startswith(name1)

def get_sheet():
    '''Returns a openpyxl workbook. Removes disiplin fields if present.'''
    workbook = openpyxl.load_workbook(EXCEL_FILE)
    # EI = 139
    sheet = workbook[SHEET]
    if sheet['EI1'].value == 'Discipline':
        #sheet.unmerge_cells('EI1:EN1')
        #sheet.delete_cols(139, 6)
        for row_number in range(3, sheet.max_row + 1, 1):
            for col in ('EI', 'EJ', 'EK', 'EL', 'EM', 'EN'):
                sheet[col+str(row_number)] = ''
    return workbook, sheet

if DO_ACCESS:
    # Create a temp file to store an excel sheet that has only has access
    # students. This will greatly speed up run time
    big_loop.set_description("Generating Scratch access report")
    access_master_file = io.BytesIO()
    big_loop.set_description("Reloading workbook")
    access_workbook, sheet = get_sheet()
    big_loop.set_description("Preping workbooks")
    # Find the access teacher column number
    access_teacher_column = 1
    #for column_number in range(sheet.max_column):
    #    if sheet.cell(row=2, column=column_number)
    for row in sheet.iter_rows(min_row=2, max_row=2):
        for cell in row:
            #print(cell.value)
            if cell.value == 'RR Teacher' or cell.value == 'Access / AVID Teacher':
                access_teacher_column = cell.column

    for row_number in tqdm.tqdm(range(sheet.max_row + 1, 2, -1), desc="Pruning non access students"):
        # Raider report name in cell M
        #print(sheet.cell(row_number, 17).value)
        #print("Peep:", sheet.cell(row_number, 17).value)
        #if sheet.cell(row_number, 17).value not in access_names or sheet.cell(row_number, 17).value == float('nan') or sheet.cell(row_number, 17).value is None:
        if not is_access(sheet.cell(row_number, access_teacher_column).value) or sheet.cell(row_number, access_teacher_column).value == float('nan') or sheet.cell(row_number, access_teacher_column).value is None:
            sheet.delete_rows(row_number)
        #else:
        #    print('Not deleting', row_number)
    access_workbook.save(access_master_file)
    access_workbook.save('output/access_yellow_report/access.xlsx')
    access_workbook.close()

    for teacher_name in tqdm.tqdm(access_names, desc="Generating access/avid yellow sum reports."):
        # Create workbooks
        #partner_workbook = copy.deepcopy(workbook)
        teacher_workbook = openpyxl.load_workbook(access_master_file)
        teacher_sheet = teacher_workbook[SHEET]
        teacher_sheet.title = str(teacher_name) + ' Students'
        # Add code here to delete suspension columns 
        # ###
        for row_number in tqdm.tqdm(range(teacher_sheet.max_row, 2, -1), desc="Pruning unwanted students"):
            #print(sheet.cell(row_number, 17).value)
            #if sheet.cell(row_number, 17).value != teacher_name:
            if not is_name(teacher_name, teacher_sheet.cell(row_number, access_teacher_column).value):
                teacher_sheet.delete_rows(row_number)
        teacher_workbook.save('output/access_yellow_report/' + str(teacher_name) + '.xlsx')

        # TODO: Iterate the rows of the prunned access_workbook just above
        # using for index, row in full_data.iterrows(): near the top. This way I
        # wont have to deal with the middle initial sadness in RR Teacher.
        document = Document()
        document.add_heading('Student report for ' + str(teacher_name))
        #document.add_paragraph('Note: Rewrite to more fancy diplomatic not 10pm at night language: The following students are currently in the yellow zone (failing 2-3 classes), including your course. The hope is we can target these students and return them to the green.')
        document.add_paragraph("I'm a bot. Beep Boop. The following students are in the yellow zone.")

        #teacher_set = full_data[full_data['Access / AVID Teacher'] == teacher_name]
        #teacher_set = full_data[full_data['RR Teacher'] == teacher_name]
        teacher_set = pd.read_excel('output/access_yellow_report/' + str(teacher_name) + '.xlsx', header=1)
        #print('Teacher_set:')
        #print(teacher_set)
        for period in tqdm.tqdm(PERIODS, desc="Processing periods"):
            #class_set = teacher_set[teacher_set['Access / AVID Period'] == period]
            #print(type(teacher_set['RR Period']))
            class_set = teacher_set[teacher_set['RR Period'] == period]
            #print('Class set:')
            #print(class_set)
            if not class_set.empty:
                class_set_yellow = class_set[class_set['Sem {} Quarter {} Grades Zone'.format(SEMESTER, QUARTER)] == 'Yellow']
                #print('Class set yellow:')
                #print(class_set_yellow)
                if not class_set_yellow.empty:
                    document.add_heading('Period ' + str(period) + ':')
                    table = document.add_table(rows = 1, cols = 6)
                    table.rows[0].cells[0].text = "Student"
                    table.rows[0].cells[1].text = "ID"
                    table.rows[0].cells[2].text = "Class"
                    table.rows[0].cells[3].text = "Teacher"
                    table.rows[0].cells[4].text = "Period"
                    table.rows[0].cells[5].text = "Grade"
                    for index, row in class_set_yellow.iterrows():
                        for class_period in PERIODS:
                            if row['P{} S{} Mark{}'.format(class_period, SEMESTER, QUARTER)] == 'F':
                                table_row = table.add_row()
                                table_row.cells[0].text = row['Student Name']
                                table_row.cells[1].text = str(row['Student ID'])
                                table_row.cells[2].text = row['P{} S{} Course'.format(class_period, SEMESTER)]
                                table_row.cells[3].text = row['P{} S{} Teacher'.format(class_period, SEMESTER)]
                                table_row.cells[4].text = str(class_period)
                                table_row.cells[5].text = row['P{} S{} Mark{}'.format(class_period, SEMESTER, QUARTER)]
                                #print(row['Student Name'], row['Student ID'], row['P{} S{} Mark{}'.format(class_period, SEMESTER, QUARTER)])
        document.save('output/access_yellow_report/' + str(teacher_name) + '.docx')

    big_loop.update()

if DO_COMMUNITY:
    # Create a temp file to store an excel sheet that has only has community partner
    # students. This will greatly speed up run time
    community_master_sheet = io.BytesIO()
    big_loop.set_description("Reloading workbook")
    partner_workbook, sheet = get_sheet()
    big_loop.set_description("Preping workbook")
    for row_number in tqdm.tqdm(range(sheet.max_row + 1, 2, -1), desc="Pruning non partnered students"):
        # Community partner name in cell M
        if sheet.cell(row_number, 13).value not in community_partners or sheet.cell(row_number, 13).value == float('nan') or sheet.cell(row_number, 13).value is None:
            sheet.delete_rows(row_number)
    partner_workbook.save(community_master_sheet)
    partner_workbook.close()

    big_loop.set_description("Generating Community Partners Reports")
    partner_loop = tqdm.tqdm(total=len(community_partners))
    for partner in community_partners:
        partner_loop.set_description("Building reports for " + str(partner))
        # Create workbooks
        #partner_workbook = copy.deepcopy(workbook)
        partner_workbook = openpyxl.load_workbook(community_master_sheet)
        sheet = partner_workbook[SHEET]
        sheet.title = str(partner) + ' Students'
        #sheet.conditional_formatting.update(workbook[SHEET].conditional_formatting.cf_rules)
        #for f in workbook[SHEET].conditional_formatting:
        #    sheet.conditional_formatting.add(f)
        '''for i in range(1, 2, 1):
            for j in range(1, 208):
                sheet.cell(row = i, column = j).value = workbook[SHEET].cell(row = i, column = j).value
                from openpyxl.cell.cell import MergedCell
                if type(workbook[SHEET].cell(row = i, column = j)) == MergedCell:
                    print(dir(workbook[SHEET].cell(row = i, column = j)))
                if workbook[SHEET].cell(row = i, column = j).has_style:
                    sheet.cell(row = i, column = j).font = copy.copy(workbook[SHEET].cell(row = i, column = j).font)
                    sheet.cell(row = i, column = j).border = copy.copy(workbook[SHEET].cell(row = i, column = j).border)
                    sheet.cell(row = i, column = j).fill = copy.copy(workbook[SHEET].cell(row = i, column = j).fill)
                    sheet.cell(row = i, column = j).number_format = copy.copy(workbook[SHEET].cell(row = i, column = j).number_format)
                    sheet.cell(row = i, column = j).protection = copy.copy(workbook[SHEET].cell(row = i, column = j).protection)
                    sheet.cell(row = i, column = j).alignment = copy.copy(workbook[SHEET].cell(row = i, column = j).alignment)
        partner_workbook.save(partner + '.xlsx')'''

        # Add code here to delete suspension columns 
        # ###
        for row_number in tqdm.tqdm(range(sheet.max_row, 2, -1), desc="Pruning unwanted students"):
            # Community partner name in cell M
            if sheet.cell(row_number, 13).value != partner:
                sheet.delete_rows(row_number)
        partner_workbook.save('output/community_partners/' + str(partner) + '.xlsx')
        
        document = Document()
        document.add_heading('Yellow Zone student report for ' + str(partner))
        # document.add_paragraph('Note: Rewrite to more fancy diplomatic not 10pm at night language: The following students are currently in the yellow zone (failing 2-3 classes), including your course. The hope is we can target these students and return them to the green.')
        document.add_paragraph("I'm a bot. Beep Boop. The following students are in the yellow zone.")
        partner_set = full_data[full_data['Community Partner Organization'] == partner]
        class_set_yellow = partner_set[partner_set['Sem {} Quarter {} Grades Zone'.format(SEMESTER, QUARTER)] == 'Yellow']
        table = document.add_table(rows = 1, cols = 6)
        table.rows[0].cells[0].text = "Student"
        table.rows[0].cells[1].text = "ID"
        table.rows[0].cells[2].text = "Class"
        table.rows[0].cells[3].text = "Teacher"
        table.rows[0].cells[4].text = "Period"
        table.rows[0].cells[5].text = "Grade"
        for index, row in class_set_yellow.iterrows():
            for class_period in PERIODS:
                if row['P{} S{} Mark{}'.format(class_period, SEMESTER, QUARTER)] == 'F':
                    table_row = table.add_row()
                    table_row.cells[0].text = row['Student Name']
                    table_row.cells[1].text = str(row['Student ID'])
                    table_row.cells[2].text = row['P{} S{} Course'.format(class_period, SEMESTER)]
                    table_row.cells[3].text = row['P{} S{} Teacher'.format(class_period, SEMESTER)]
                    table_row.cells[4].text = str(class_period)
                    table_row.cells[5].text = row['P{} S{} Mark{}'.format(class_period, SEMESTER, QUARTER)]
        document.save('output/community_partners/' + str(partner) + '.docx')
        partner_loop.update()
    partner_loop.close()

big_loop.update()

if DO_TEACHER:
    big_loop.set_description("Creating individual teacher reports.")
    for teacher_name in tqdm.tqdm(teacher_names, desc="Generating teacher yellow log."):
        big_loop.set_description("Creating individual teacher reports: " + teacher_name)
        #print("Processing: ", teacher_name)
        yellow_report = []
        document = Document()
        document.add_heading('Student report for ' + str(teacher_name))
        #document.add_paragraph('Note: Rewrite to more fancy diplomatic not 10pm at night language: The following students are currently in the yellow zone (failing 2-3 classes), including your course. The hope is we can target these students and return them to the green.')
        document.add_paragraph("I'm a bot. Beep Boop. The following students are in the yellow zone in reguards to your class.")
        for period in PERIODS:
            class_set = full_data[full_data['P{} S{} Teacher'.format(period, SEMESTER)] == teacher_name]
            class_set_red = class_set[class_set['Sem {} Quarter {} Grades Zone'.format(SEMESTER, QUARTER)] == 'Red']
            class_set_yellow = class_set[class_set['Sem {} Quarter {} Grades Zone'.format(SEMESTER, QUARTER)] == 'Yellow']
            class_set_green = class_set[class_set['Sem {} Quarter {} Grades Zone'.format(SEMESTER, QUARTER)] == 'Green']
            yellow_inclass = class_set_yellow[class_set_yellow["P{} S{} Mark{}".format(period, SEMESTER, QUARTER)] == 'F']
            students = list(yellow_inclass['Student Name'].values)
            if students:
                #print("\tPeriod:", period)
                document.add_heading('Period ' + str(period) + ':')
            for student in students:
                #print("\t\tStudent:", student)
                document.add_paragraph(student)
        
        document.save('output/teacher_yellow_report/' + str(teacher_name) + '.docx')

    big_loop.update()

if DO_COUSELING:
    big_loop.set_description("Creating counseling reports.")
    counseling_master_sheet = io.BytesIO()
    for counselor_name in counselor_names:
        big_loop.set_description("Reloading workbook")
        counseling_workbook, sheet = get_sheet()
        big_loop.set_description("Preping workbooks")
        big_loop.set_description("Creating individual counseler reports: " + counselor_name)
        for row_number in tqdm.tqdm(range(sheet.max_row, 2, -1), desc="Pruning unwanted students"):
            #print(sheet.cell(row_number, 14).value, counselor_name)
            #if not is_name(counselor_name, sheet.cell(row_number, 14).value):
            #print(sheet.cell(row_number, 14).value != counselor_name, sheet.cell(row_number, 14).value, "|", counselor_name)
            if sheet.cell(row_number, 14).value != counselor_name:
                #print(sheet.cell(row_number, 14).value, counselor_name)
                sheet.delete_rows(row_number)
        #counseling_workbook.security.workbookPassword('ra1der')
        counseling_workbook.save('output/counselor_report/' + str(counselor_name) + '.xlsx')