from copy import copy
import io
import logging
import os
import sys
import tempfile
import time

import openpyxl

import PyPDF2
from PySide6 import QtCore, QtGui, QtSvg, QtWidgets
from reportlab.pdfgen import canvas
from reportlab.lib import colors
from reportlab.lib.pagesizes import letter
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import inch
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Table

from raiderreporter import factory, message, threadpool, WEEKS, Worker, gui, sharepoint
from raiderreporter.gui import EarlyWarningDropLoadWidget, EarlyWarningLoadingWidget

# logging.basicConfig(level=logging.DEBUG, filename='debug.log')


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    base_path = getattr(sys, '_MEIPASS', os.path.dirname(os.path.abspath(__file__)))
    return os.path.join(base_path, relative_path)


class DataScreen(QtWidgets.QGridLayout):
    def __init__(self):
        QtWidgets.QGridLayout.__init__(self)
        # background = QtSvg.QSvgWidget('Raider face.svg')
        # self.addWidget(background, 0, 0)
        self.addWidget(QtWidgets.QLabel("Create report?"), 0, 1)
        self.addWidget(QtWidgets.QLabel("Password? Blank for none"), 0, 2)
        self.addWidget(QtWidgets.QLabel("Email report? (Not functional... yet)"), 0, 3)
        self.addWidget(QtWidgets.QLabel("Access reports"), 1, 0)
        self.addWidget(QtWidgets.QLabel("Counselor reports"), 2, 0)
        self.addWidget(QtWidgets.QLabel("Teacher zone and eld reports. (Excel split disabled.)"), 3, 0)
        self.addWidget(QtWidgets.QLabel("Community partner reports"), 4, 0)
        for row in range(1, 5):
            for column in (1, 3):
                self.addWidget(QtWidgets.QCheckBox(), row, column)
        # Password field
        self.addWidget(QtWidgets.QLineEdit(), 1, 2)
        self.addWidget(QtWidgets.QLineEdit(), 3, 2)
        self.button = QtWidgets.QPushButton("Run! (Seriously this takes a while. Get some coffee.)")
        self.button.clicked.connect(self.run_reports)
        self.addWidget(self.button, 5, 0)
    
    @QtCore.Slot()
    def run_reports(self):
        eng = factory.get_engine()
        run_access = self.itemAtPosition(1, 1).widget().isChecked()
        run_counselor = self.itemAtPosition(2, 1).widget().isChecked()
        run_teacher = self.itemAtPosition(3, 1).widget().isChecked()
        run_partner = self.itemAtPosition(4, 1).widget().isChecked()
        for x in range(0, 5):
            for y in range(1, 4):
                if self.itemAtPosition(x, y):
                    self.itemAtPosition(x, y).widget().hide()
        self.itemAtPosition(1, 1).widget().hide()
        if run_access:
            self.access_progress = QtWidgets.QProgressBar()
            self.access_progress.setMaximum(0)
            self.access_progress.setMinimum(0)
            self.addWidget(self.access_progress, 1, 1, 1, 2)
            message.set_access_bar_max.connect(self.set_max_access)
            message.tick_access_bar.connect(self.tick_access)
            password = self.itemAtPosition(1, 2).widget().text()
            worker = Worker(eng.access_reports, password=password)
            threadpool.start(worker)
        else:
            #self.replacewidget?
            self.addWidget(QtWidgets.QLabel("Not selected."), 1, 1, 1, 2)
        if run_counselor:
            self.counselor_progress = QtWidgets.QProgressBar()
            self.counselor_progress.setMaximum(0)
            self.counselor_progress.setMinimum(0)
            self.addWidget(self.counselor_progress, 2, 1, 1, 2)
            message.set_counselor_bar_max.connect(self.set_max_counselor)
            message.tick_counselor_bar.connect(self.tick_counselor)
            worker = Worker(eng.counseling_reports)
            threadpool.start(worker)
        else:
            self.addWidget(QtWidgets.QLabel("Not selected."), 2, 1, 1, 2)
        if run_teacher:
            self.teacher_progress = QtWidgets.QProgressBar()
            self.teacher_progress.setRange(0, 0)
            self.addWidget(self.teacher_progress, 3, 1, 1, 2)
            message.set_teacher_bar_max.connect(self.set_max_teacher)
            message.tick_teacher_bar.connect(self.tick_teacher)
            password = self.itemAtPosition(3, 2).widget().text()
            email = self.itemAtPosition(3, 3).widget().isChecked()
            worker = Worker(eng.teacher_reports, password=password, pdf=True, excel=False, email=email)
            threadpool.start(worker)
        else:
            self.addWidget(QtWidgets.QLabel("Not selected."), 3, 1, 1, 2)
        if run_partner:
            self.partner_progress = QtWidgets.QProgressBar()
            self.partner_progress.setMaximum(0)
            self.partner_progress.setMinimum(0)
            self.addWidget(self.partner_progress, 4, 1, 1, 2)
            message.set_partner_bar_max.connect(self.set_max_partner)
            message.tick_partner_bar.connect(self.tick_partner)
            worker = Worker(eng.community_reports)
            threadpool.start(worker)
        else:
            self.addWidget(QtWidgets.QLabel("Not selected."), 4, 1, 1, 2)
        self.button.setDisabled(True)

    @QtCore.Slot(int)
    def set_max_access(self, value):
        self.access_progress.setMaximum(value)
    
    @QtCore.Slot(int)
    def set_max_counselor(self, value):
        self.counselor_progress.setMaximum(value)
    
    @QtCore.Slot(int)
    def set_max_teacher(self, value):
        self.teacher_progress.setMaximum(value)
    
    @QtCore.Slot(int)
    def set_max_partner(self, value):
        self.partner_progress.setMaximum(value)
    
    @QtCore.Slot()
    def tick_access(self):
        self.access_progress.setValue(self.access_progress.value() + 1)

    @QtCore.Slot()
    def tick_counselor(self):
        self.counselor_progress.setValue(self.counselor_progress.value() + 1)

    @QtCore.Slot()
    def tick_teacher(self):
        self.teacher_progress.setValue(self.teacher_progress.value() + 1)

    @QtCore.Slot()
    def tick_partner(self):
        self.partner_progress.setValue(self.partner_progress.value() + 1)


class EarlyWarningDataScreen(QtWidgets.QGridLayout):
    def __init__(self):
        QtWidgets.QGridLayout.__init__(self)
        # background = QtSvg.QSvgWidget('Raider face.svg')
        # self.addWidget(background, 0, 0)
        self.convert_button = QtWidgets.QPushButton("Convert Report.")
        self.convert_button.clicked.connect(self.convert_report)
        self.addWidget(self.convert_button, 1, 0)
        self.addWidget(QtWidgets.QPushButton("Generate partial raider report. Not functional. Yet."), 2, 0)
        self.addWidget(QtWidgets.QPushButton("Generate sql database. Not functional. Yet."), 3, 0)
        print("Finished generating data screen")
    
    @QtCore.Slot()
    def convert_report(self):
        eng = factory.eng2
        dialog = QtWidgets.QFileDialog()
        dialog.setFileMode(QtWidgets.QFileDialog.AnyFile)
        dialog.setNameFilter(self.tr("Excel (*.xlsx)"))
        dialog.setViewMode(QtWidgets.QFileDialog.Detail)
        dialog.setWindowTitle("Save converted file as....")
        if dialog.exec_():
            filename = dialog.selectedFiles()[0]
            if not filename.endswith('.xlsx'):
                filename = filename + '.xlsx'
        else:
            print("CRITICAL ERROR! NO SAVE AS SELECTED")

        # self.itemAtPosition(0, 0).widget().hide()
        self.convert_progress = QtWidgets.QProgressBar()
        self.convert_progress.setMaximum(0)
        self.convert_progress.setMinimum(0)
        self.addWidget(self.convert_progress, 1, 1, 1, 2)
        message.set_convert_bar_max.connect(self.set_max_convert)
        message.tick_convert_bar.connect(self.tick_convert)
        worker = Worker(eng.convert_report, output_filename=filename, week=self.week)
        threadpool.start(worker)
        self.convert_button.setDisabled(True)

    @QtCore.Slot(int)
    def set_max_convert(self, value):
        self.convert_progress.setMaximum(value)
    
    @QtCore.Slot()
    def tick_convert(self):
        self.convert_progress.setValue(self.convert_progress.value() + 1)


class DropLoadWidget(QtWidgets.QWidget):
    def __init__(self):
        QtWidgets.QWidget.__init__(self)
        self.setLayout(QtWidgets.QVBoxLayout())
        self.setAcceptDrops(True)
        #path = resource_path('Raider face.svg')
        #background = QtSvg.QSvgRenderer(path)
        #background.render(self)
        #background = QtSvg.QSvgWidget(path)
        #self.layout().addWidget(background)
        label = QtWidgets.QLabel()
        label.setText("Select what report engine to use.")
        self.layout().addWidget(label)
        self.engine_select = QtWidgets.QComboBox()
        self.engine_select.addItem("Engine Version 1 2023 Edition")
        self.engine_select.addItem("Engine Version 1 2020 Edition")
        self.engine_select.addItem("Engine Version 1")
        self.engine_select.addItem("NOT READY FOR PRODUCTION: SQL Engine 'Version 2'")
        self.engine_select.addItem("NOT READY FOR PRODUCTION: Pandas Engine 'Version 3'")
        self.engine_select.addItem("NOT READY FOR PRODUCTION: Early Warning 'Version 1 2023'")
        self.layout().addWidget(self.engine_select)
        label = QtWidgets.QLabel()
        label.setText("Please drag the Excel student report here.")       
        self.layout().addWidget(label)

    def dragEnterEvent(self, e):
        if e.mimeData().hasUrls():
            e.acceptProposedAction()

    def dropEvent(self, e):
        factory.set_engine(self.engine_select.currentIndex() + 1)
        eng = factory.get_engine()
        for url in e.mimeData().urls():
            message.set_status.emit("Loading file and checking things out. This can take a while.")
            file_name = url.toLocalFile()
            worker = Worker(eng.load, file_name)
            threadpool.start(worker)
        self.setAcceptDrops(False)


class SelectSheetWidget(QtWidgets.QWidget):
    def __init__(self):
        QtWidgets.QWidget.__init__(self)
        self.setLayout(QtWidgets.QVBoxLayout())
        self.setAcceptDrops(True)
        #path = resource_path('Raider face.svg')
        #background = QtSvg.QSvgWidget(path)
        #self.layout().addWidget(background)
        label = QtWidgets.QLabel()
        label.setText("Please select sheet to load")
        self.layout().addWidget(label)
        self.dropdown = QtWidgets.QComboBox()
        self.layout().addWidget(self.dropdown)
        self.week_select = QtWidgets.QComboBox()
        label = QtWidgets.QLabel()
        label.setText("Select what week the imported student report is for.")
        self.layout().addWidget(label)
        for week in WEEKS:
            self.week_select.addItem(f"Week {week[0]}, Quarter {week[1]}, Semester {week[2]}")
        self.layout().addWidget(self.week_select)
        self.button = QtWidgets.QPushButton("Load")
        self.layout().addWidget(self.button)
    
    def select_sheet(self, name_list, finish_loading_function):
        self.dropdown.addItems(name_list)
        self.button.clicked.connect(finish_loading_function)


class SlicerWidget(QtWidgets.QStackedWidget):
    def __init__(self):
        QtWidgets.QStackedWidget.__init__(self)
        self.addWidget(DropLoadWidget())
        self.select_sheet_widget = SelectSheetWidget()
        self.addWidget(self.select_sheet_widget)
        datascreen_widget = QtWidgets.QWidget()
        datascreen_widget.setLayout(DataScreen())
        self.addWidget(datascreen_widget)
        message.finished_inital_extraction.connect(self.finish_init_load)

    @QtCore.Slot()
    def finish_init_load(self):
        eng = factory.get_engine()
        self.select_sheet_widget.select_sheet(eng.return_sheets(), self.finish_loading)
        self.setCurrentIndex(1)
    
    @QtCore.Slot()
    def finish_loading(self):
        eng = factory.get_engine()
        excel_sheet = self.select_sheet_widget.dropdown.currentText()
        week, quarter, semester = WEEKS[self.select_sheet_widget.week_select.currentIndex()]
        worker = Worker(eng.load_sheet, excel_sheet, semester=semester, quarter=quarter)
        threadpool.start(worker)
        message.super_finished_initial_extraction.connect(self.super_finished_loading)
    
    @QtCore.Slot()
    def super_finished_loading(self):
        self.setCurrentIndex(2)


class EarlyWarningConverterWidget(QtWidgets.QStackedWidget):
    def __init__(self):
        QtWidgets.QStackedWidget.__init__(self)
        self.addWidget(EarlyWarningDropLoadWidget())
        self.addWidget(EarlyWarningLoadingWidget())
        self.datascreen_widget = QtWidgets.QWidget()
        self.datascreen_widget.setLayout(EarlyWarningDataScreen())
        self.addWidget(self.datascreen_widget)
        #message.finished_csv_extraction.connect(self.finish_init_load)
        message.start_early_warning_import.connect(self.begin_earlywarning_imports)
        message.earlywarning_import_complete.connect(self.super_finished_loading)
    
    @QtCore.Slot()
    def begin_earlywarning_imports(self, file_paths, week, quarter, semester):
        self.setCurrentIndex(1)
        eng = factory.eng2
        self.week = week
        worker = Worker(eng.load_earlywarning_reports, file_paths, week=week, quarter=quarter, semester=semester)
        threadpool.start(worker)
    
    @QtCore.Slot()
    def super_finished_loading(self):
        self.datascreen_widget.layout().week = self.week
        self.setCurrentIndex(2)
        

class TabbedWidget(QtWidgets.QWidget):
    def __init__(self):
        QtWidgets.QWidget.__init__(self)
        self.layout = QtWidgets.QVBoxLayout()
        # Setup Tabs
        self.tabs = QtWidgets.QTabWidget()
        self.tabSlicer = SlicerWidget()
        self.tabReorder = EarlyWarningConverterWidget()
        self.tabViewer = QtWidgets.QWidget()
        self.tabs.addTab(self.tabSlicer, "Report Slicer")
        self.tabs.addTab(self.tabReorder, "Early Warning Converter")
        self.tabs.addTab(self.tabViewer, "Database Viewer")
        url = sharepoint.get_authenticate_url()
        self.tabs.addTab(gui.BrowserTab(url), "Browser Test")
        self.layout.addWidget(self.tabs)
        self.setLayout(self.layout)


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        central = TabbedWidget()
        self.setCentralWidget(central)
        central.show()

        self.progress_bar = QtWidgets.QProgressBar()
        self.status = self.statusBar()
        self.status.addPermanentWidget(self.progress_bar)

        message.set_main_bar_max.connect(self.set_main_progress_max)
        message.tick_main_bar.connect(self.tick_main_bar)
        message.zero_main_bar.connect(self.zero_main_bar)
        message.finished_inital_extraction.connect(self.finish_init_load)
        #self.set_status("Please drag Summers Excel student report here.")
        self.resize(800, 500)
    
    @QtCore.Slot()
    def finish_init_load(self):
        self.clear_status()
    
    @QtCore.Slot(str)
    def set_status(self, message):
        #print("Status:", message)
        logging.info(message)
        self.status.showMessage(message)
    
    @QtCore.Slot(str)
    def set_warning(self, message):
        print("Warning:", message)
        logging.warning(message)
        self.status.showMessage(message)
    
    @QtCore.Slot()
    def clear_status(self):
        self.status.clearMessage()
    
    @QtCore.Slot(int)
    def set_main_progress_max(self, value):
        self.progress_bar.setMaximum(value)
    
    @QtCore.Slot()
    def tick_main_bar(self):
        self.progress_bar.setValue(self.progress_bar.value() + 1)
    
    @QtCore.Slot()
    def zero_main_bar(self):
        self.progress_bar.setRange(0, 0)


app = QtWidgets.QApplication(sys.argv)
app.setApplicationDisplayName('Raider Reporter')
window = MainWindow()
print("Hello! This is a debug window. If any errors happen they will pop up here!")
message.set_status.connect(window.set_status)
message.clear_status.connect(window.clear_status)
window.show()
app.exec()