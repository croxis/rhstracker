import enum

from sqlalchemy import create_engine
from sqlalchemy import Boolean, Column, Enum, Float, ForeignKey, Integer, String, Table
from sqlalchemy import Sequence
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, scoped_session, sessionmaker
from sqlalchemy.pool import StaticPool
from sqlalchemy.sql import exists

#engine = create_engine('sqlite:///:memory:', connect_args={'check_same_thread': False}, echo=True, poolclass=StaticPool)
engine = create_engine('sqlite:///:memory:', connect_args={'check_same_thread': False}, poolclass=StaticPool)
#engine = create_engine('sqlite:///D:/tmp/database.db', connect_args={'check_same_thread': False}, poolclass=StaticPool)
#engine = create_engine('sqlite:///database.db', connect_args={'check_same_thread': False}, poolclass=StaticPool)

Base = declarative_base()
session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)
local_session = Session()

class SportSeason(enum.Enum):
    FALL = 1
    WINTER = 2
    SPRING = 3


def get_or_create(session, model, **kwargs):
    instance = session.query(model).filter_by(**kwargs).first()
    if instance:
        return instance
    else:
        instance = model(**kwargs)
        session.add(instance)
        session.commit()
        return instance

def split_name(raw_name: str):
    try:
        last_name, first_raw = raw_name.split(', ')
        first_name = ''
        middle_initial = None
        if first_raw[-1] == '.':
            first_name = first_raw[:-3]
            middle_initial = first_raw[-2:-1]
        else:
            first_name = first_raw
        return last_name, first_name, middle_initial
    except Exception as e:
        print("Error with:", raw_name)
        print(e)
        raise

# Table for the manytomany class-students
class_student = Table('class_student', Base.metadata,
                      Column('student_id', ForeignKey('students.id'), primary_key=True),
                      Column('class_id', ForeignKey('classes.id'), primary_key=True))

# Table for the manytomany community-students
community_student = Table('community_student', Base.metadata,
                      Column('student_id', ForeignKey('students.id'), primary_key=True),
                      Column('community_id', ForeignKey('community.id'), primary_key=True))

# Table for the manytomany sports-students
sport_student = Table('sport_student', Base.metadata,
                      Column('student_id', ForeignKey('students.id'), primary_key=True),
                      Column('sport_id', ForeignKey('sports.id'), primary_key=True))


class Community(Base):
    __tablename__ = 'community'
    id = Column(Integer, Sequence('community_id_seq'), primary_key=True)
    name = Column(String(5))
    students = relationship("Student", secondary=community_student, back_populates="partners", lazy='dynamic')


class Subject(Base):
    __tablename__ = 'subjects'
    id = Column(Integer, Sequence('subject_id_seq'), primary_key=True)
    name = Column(String(5))

    def __repr__(self):
        return f"<Subject(name={self.name})>"


class Course(Base):
    __tablename__ = 'courses'
    id = Column(Integer, Sequence('course_id_seq'), primary_key=True)
    name = Column(String(50))
    subject_id = Column(Integer, ForeignKey('subjects.id'))
    subject = relationship("Subject", foreign_keys=subject_id)
    def __repr__(self):
        return f"<Course(name={self.name})>"


class Class(Base):
    __tablename__ = 'classes'
    id = Column(Integer, Sequence('class_id_seq'), primary_key=True)
    course_id = Column(Integer, ForeignKey('courses.id'))
    course = relationship("Course", foreign_keys=course_id)
    period = Column(Integer)
    semester = Column(Integer)
    year = Column(Integer)  # School year start for this class
    teacher_id = Column(Integer, ForeignKey('staff.id'))
    teacher = relationship("Staff", foreign_keys=teacher_id, back_populates="classes")
    students = relationship('Student', secondary=class_student, back_populates='classes', lazy='dynamic')

    def __repr__(self):
        return f"<Class(name={self.course.name}, period={self.period}, teacher={self.teacher.get_name()}, database_id={self.id})>" 
    
    def get_report_name(self) -> str:
        return f'{self.period}-{self.course.name}'


class Sport(Base):
    __tablename__ = 'sports'
    id = Column(Integer, Sequence('sport_id_seq'), primary_key=True)
    name = Column(String(50))
    season = Column(Enum(SportSeason))
    year = Column(Integer)  # School year start for this class
    students = relationship('Student', secondary=sport_student, back_populates='sports', lazy='dynamic')

    def __repr__(self):
        return f"<Sport(name={self.name}, season={self.season}, database_id={self.id})>" 


class Attendance(Base):
    __tablename__ = 'attendance'
    id = Column(Integer, Sequence('attendance_id_seq'), primary_key=True)
    student_id = Column(Integer, ForeignKey('students.id'))
    year = Column(Integer)
    sept = Column(Float)
    octo = Column(Float)
    nov = Column(Float)
    dec = Column(Float)
    jan = Column(Float)
    feb = Column(Float)
    mar = Column(Float)
    apr = Column(Float)
    may = Column(Float)
    jun  = Column(Float)
    ytd_attn = Column(Float)
    
    def __repr__(self) -> str:
        return f"<Attendance(Student ID={self.student_id}, Sept={self.sept}, Oct={self.octo})>"


class GPA(Base):
    __tablename__ = 'gpa'
    id = Column(Integer, Sequence('gpa_id_seq'), primary_key=True)
    student_id = Column(Integer, ForeignKey('students.id'))
    week = Column(Integer)
    year = Column(Integer)
    quarter1_gpa = Column(Float)
    quarter2_gpa = Column(Float)
    quarter3_gpa = Column(Float)
    quarter4_gpa = Column(Float)


class GradeEntry(Base):
    __tablename__ = 'class_grades'
    id = Column(Integer, Sequence('gradeentry_id_seq'), primary_key=True)
    student_id = Column(Integer, ForeignKey('students.id'))
    class_id = Column(Integer, ForeignKey('classes.id'))
    week = Column(Integer)
    quarter1_grade = Column(String(2))
    quarter2_grade = Column(String(2))
    quarter3_grade = Column(String(2))
    quarter4_grade = Column(String(2))
    quarter1_abs = Column(Integer)
    quarter2_abs = Column(Integer)
    quarter3_abs = Column(Integer)
    quarter4_abs = Column(Integer)


class Incident(Base):
    __tablename__ = 'incidents'
    id = Column(Integer, Sequence('gradeentry_id_seq'), primary_key=True)
    student_id = Column(Integer, ForeignKey('students.id'))
    year = Column(Integer)  # School year start for this entry
    week = Column(Integer)
    incident_total = Column(Integer)
    iss_total = Column(Integer)
    iss_days = Column(Integer)
    oss_total = Column(Integer)
    oss_days = Column(Integer)
    exp_total = Column(Integer)


class Staff(Base):
    __tablename__ = 'staff'
    id = Column(Integer, Sequence('staff_id_seq'), primary_key=True)
    first_name = Column(String(50))
    last_name = Column(String(50))
    middle_initial = Column(String(5))
    classes = relationship("Class", order_by=Class.period)

    def get_O365_name(self) -> str:
        '''returns firstname last name with aposterphies removed'''
        # last, first = name.split('\\')[1].split('.docx')[0].replace("'", '').split(', ')
        return self.first_name.replace("'", '') + ' ' + self.last_name.replace("'", '')
    
    def get_email(self) -> str:
        email = self.first_name[0].lower() + self.last_name.lower() + '@rsd7.net'
        return email
    
    def get_name(self) -> str:
        return self.last_name + ', ' + self.first_name

    def __repr__(self):
        return "<Staff(name='%s', '%s')>" % (self.last_name, self.first_name)


class Student(Base):
    __tablename__ = 'students'
    id = Column(Integer, primary_key=True)
    first_name = Column(String(50))
    last_name = Column(String(50))
    # The nineth grade start year is used because schoolastic calendars suck. Senior start year is +3 years. Grad year is an additional 4.
    nineth_grade_year = Column(Integer)
    gender = Column(String(3))
    race = Column(String(50))
    ELL = Column(String(50))
    five_oh_four = Column(Boolean, default=False)
    sped = Column(Boolean, default=False)
    diploma = Column(String(30)) # TODO: Put this in a table
    counselor_id = Column(Integer, ForeignKey('staff.id'))
    counselor = relationship("Staff", foreign_keys=counselor_id)
    sped_case_id = Column(Integer, ForeignKey('staff.id'))
    sped_case = relationship("Staff", foreign_keys=sped_case_id)
    house = Column(String(50)) # TODO Put in table
    migrant = Column(Boolean, default=False)
    tag = Column(Boolean, default=False)
    language_to_home = Column(String)
    dropped = Column(Boolean, default=False)

    classes = relationship('Class', secondary=class_student, back_populates='students', lazy='dynamic')
    partners = relationship("Community", secondary=community_student, back_populates="students", lazy='dynamic')
    sports = relationship("Sport", secondary=sport_student, back_populates="students", lazy='dynamic')
    
    def __repr__(self):
        return "<Student(name='%s', '%s'; id='%i')>" % (self.last_name, self.first_name, self.id)
    
    def get_name(self):
        return self.last_name + ', ' + self.first_name

Base.metadata.create_all(engine)