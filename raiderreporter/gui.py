from PySide6 import QtCore, QtGui, QtNetwork, QtWebEngineCore, QtWebEngineWidgets, QtWidgets
from . import factory, message, threadpool, WEEKS, Worker


class FileWidget(QtWidgets.QWidget):
    def __init__(self, url):
        QtWidgets.QWidget.__init__(self)
        self.url = url
        layout = QtWidgets.QHBoxLayout()
        label = QtWidgets.QLabel(self.url.toString())
        #label.text = self.path

        layout.addWidget(label)

        close_button = QtWidgets.QPushButton('X')
        close_button.clicked.connect(self.close)

        layout.addWidget(close_button)

        self.setLayout(layout)
    
    @QtCore.Slot()
    def close(self):
        self.deleteLater()


class EarlyWarningLoadingWidget(QtWidgets.QWidget):
    def __init__(self):
        QtWidgets.QWidget.__init__(self)
        label = QtWidgets.QLabel("Loading early warning reports...")
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(label)
        self.setLayout(layout)


class EarlyWarningDropLoadWidget(QtWidgets.QWidget):
    def __init__(self):
        QtWidgets.QWidget.__init__(self)
        self.top_layout = QtWidgets.QGridLayout()
        self.button_layout = QtWidgets.QVBoxLayout()
        widget = QtWidgets.QWidget()
        widget.setLayout(self.button_layout)
        self.top_layout.addWidget(widget, 0, 0)

        self.setAcceptDrops(True)
        #path = resource_path('Raider face.svg')
        #background = QtSvg.QSvgWidget(path)
        #self.layout().addWidget(background)
        label = QtWidgets.QLabel()
        label.setText("Please drag CVS student reports here.")       
        self.top_layout.addWidget(label, 1, 0)

        self.week_select = QtWidgets.QComboBox()
        for week in WEEKS:
            self.week_select.addItem(f"Week {week[0]}, Quarter {week[1]}, Semester {week[2]}")
        self.top_layout.addWidget(self.week_select, 2, 0)
        self.button = QtWidgets.QPushButton("Load")
        self.button.clicked.connect(self.import_button_clicked)
        self.top_layout.addWidget(self.button, 3, 0)

        self.setLayout(self.top_layout)

    def dragEnterEvent(self, e):
        if e.mimeData().hasUrls():
            e.acceptProposedAction()

    def dropEvent(self, e):
        for url in e.mimeData().urls():
            file_widget = FileWidget(url)
            self.button_layout.addWidget(file_widget)
    
    def import_button_clicked(self, e):
        file_paths = []
        index = self.button_layout.count()
        for i in range(0, self.button_layout.count()):
            file_paths.append(self.button_layout.itemAt(i).widget().url.toLocalFile())
        self.setAcceptDrops(False)
        week = WEEKS[self.week_select.currentIndex()][0]
        quarter = WEEKS[self.week_select.currentIndex()][1]
        semester = WEEKS[self.week_select.currentIndex()][2]

        message.start_early_warning_import.emit(file_paths, week, quarter, semester)


class BrowserTab(QtWidgets.QWidget):
    #def __init__(self, url, container):
    def __init__(self, url):
        super(BrowserTab, self).__init__()
        #self.container = container
        self.web_view = QtWebEngineWidgets.QWebEngineView()
        self.web_view.urlChanged.connect(self.print_url)
        #self.progress_bar = QtWidgets.QProgressBar(
        #    self.container.statusBar(), maximumWidth=120, visible=False
        #)
        #self.web_view.loadProgress.connect(
        #    lambda v: (self.progress_bar.show(), self.progress_bar.setValue(v))
        #)
        #self.web_view.loadFinished.connect(self.progress_bar.hide)
        #self.web_view.loadStarted.connect(
        #    lambda: self.progress_bar.show()
        #)
        self.tb = QtWidgets.QToolBar("Main Toolbar", self)

        layout = QtWidgets.QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.tb, stretch=0)
        layout.addWidget(self.web_view, stretch=1000)
        layout.activate()
        self.setLayout(layout)
        for a, sc in [
            [QtWebEngineCore.QWebEnginePage.Back, QtGui.QKeySequence.Back],
            [QtWebEngineCore.QWebEnginePage.Forward, QtGui.QKeySequence.Forward],
            [QtWebEngineCore.QWebEnginePage.Reload, QtGui.QKeySequence.Refresh],
        ]:
            self.tb.addAction(self.web_view.pageAction(a))
            self.web_view.pageAction(a).setShortcut(sc)

        self.url = QtWidgets.QLineEdit()
        self.url.returnPressed.connect(
            lambda: self.web_view.load(QtCore.QUrl.fromUserInput(self.url.text()))
        )
        self.tb.addWidget(self.url)

        self.web_view.urlChanged.connect(lambda u: self.url.setText(u.toString()))

        #self.web_view.page().linkHovered.connect(
        #    lambda l: container.statusBar().showMessage(l, 3000)
        #)
        self.urlFocus = QtGui.QShortcut("Ctrl+l", self, activated=self.url.setFocus)

        self.web_view.load(url)
    
    def print_url(self, url):
        if url.toString().startswith('https://login.microsoftonline.com/common/oauth2/nativeclient?code='):
            print("OH HO!:", url)