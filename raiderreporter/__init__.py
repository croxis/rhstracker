from PySide6 import QtCore


SEMESTERS = (1, 2)
PERIODS = (1, 2, 3, 4, 5, 6, 7, 8, 10)
# Week, Quarter, Semester
WEEKS = ((3, 1, 1),
         (6, 1, 1),
         (9, 1, 1),
         (12, 2, 1),
         (15, 2, 1),
         (18, 2, 1),
         (21, 3, 2),
         (24, 3, 2),
         (27, 3, 2),
         (30, 4, 2),
         (33, 4, 2),
         (36, 4, 2))

class Communicate(QtCore.QObject):
    set_status = QtCore.Signal(str)
    set_warning = QtCore.Signal(str)
    clear_status = QtCore.Signal()
    finished_inital_extraction = QtCore.Signal()
    finished_csv_extraction = QtCore.Signal()
    start_early_warning_import = QtCore.Signal(list, int, int, int)
    earlywarning_import_complete = QtCore.Signal()
    summer_super_finished_initial_extraction = QtCore.Signal()
    super_finished_initial_extraction = QtCore.Signal()
    error = QtCore.Signal(tuple)
    set_teacher_bar_max = QtCore.Signal(int)
    set_access_bar_max = QtCore.Signal(int)
    set_partner_bar_max = QtCore.Signal(int)
    set_counselor_bar_max = QtCore.Signal(int)
    set_main_bar_max = QtCore.Signal(int)
    set_convert_bar_max = QtCore.Signal(int)
    tick_teacher_bar = QtCore.Signal()
    tick_access_bar = QtCore.Signal()
    tick_partner_bar = QtCore.Signal()
    tick_counselor_bar = QtCore.Signal()
    tick_convert_bar = QtCore.Signal()
    tick_main_bar = QtCore.Signal()
    zero_main_bar = QtCore.Signal()

message = Communicate()

threadpool = QtCore.QThreadPool()
# Leave a couple thread open for the os
threads = threadpool.maxThreadCount() - 2
if threads < 1:
    threads = 1
threadpool.setMaxThreadCount(threads)
#threadpool.setMaxThreadCount(2)

print("Multithreading with a maximum of %d threads" % threadpool.maxThreadCount())

class Worker(QtCore.QRunnable):
    '''
    Worker thread

    Inherits from QRunnable to handler worker thread setup, signals and wrap-up.

    :param callback: The function callback to run on this worker thread. Supplied args and 
                     kwargs will be passed through to the runner.
    :type callback: function
    :param args: Arguments to pass to the callback function
    :param kwargs: Keywords to pass to the callback function

    https://www.learnpyqt.com/courses/concurrent-execution/multithreading-pyqt-applications-qthreadpool/

    '''
    def __init__(self, fn, *args, **kwargs):
        super(Worker, self).__init__()
        # Store constructor arguments (re-used for processing)
        self.fn = fn
        self.args = args
        self.kwargs = kwargs

    @QtCore.Slot()  # QtCore.Slot
    def run(self):
        '''
        Initialise the runner function with passed args, kwargs.
        '''
        self.fn(*self.args, **self.kwargs)

from . import engine

class EngineFactory:
    def __init__(self):
        self.eng1 = engine.ReportEngineV1()
        self.eng12020 = engine.ReportEngineV1N2020()
        self.eng12023 = engine.ReportEngineV1N2023()
        self.eng2 = engine.ReportEngineV2()
        self.eng3 = engine.ReportEngineV3()
        self.eng4 = engine.ReportEngineV1N2023EarlyWarning()
        self.active_engine = self.eng1
    
    def get_engine(self) -> engine.ReportEngineAPI:
        return self.active_engine
    
    def set_engine(self, version: int):
        if version == 1:
            self.active_engine = self.eng12023
        if version == 2:
            self.active_engine = self.eng12020
        elif version == 3:
            self.active_engine = self.eng1       
        elif version == 4:
            self.active_engine = self.eng2
        elif version == 5:
            self.active_engine = self.eng3
        elif version == 6:
            self.active_engine = self.eng4

factory = EngineFactory()


