from copy import copy
import datetime
import distutils.util
import io
import os
import time
import openpyxl
from openpyxl.cell import Cell, WriteOnlyCell
from openpyxl.formatting import Rule
from openpyxl.styles import Font, GradientFill, Alignment
from openpyxl.styles.differential import DifferentialStyle
from openpyxl.utils import get_column_letter
from openpyxl.worksheet.dimensions import ColumnDimension, DimensionHolder
import pandas as pd
#import pythoncom
import PyPDF2
#import win32com.client
from reportlab.lib import colors
from reportlab.lib.pagesizes import letter
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Table
from . import message
from . import PERIODS, SEMESTERS
from . import sql
from . import sharepoint
from . import threadpool
from . import Worker

now = datetime.datetime.now()
current_school_year = now.year
if now.month <= 8:
    current_school_year -= 1


class ReportEngineAPI():
    '''This is a classified (pun!) version of the command line version of
    the raider report. This is to create a semblence of an api for V2 and
    V3 of the engine which is sqlite based.

    Version 1 is VERY inefficent with some of the operations. This is the
    sake for coding clarity for me. My hope is version 2 and 3 (memory
    and on disk) will sacrifice some initial load time to populate the
    database with faster operations.'''
    quarter = 1
    semester = 1
    access_names = ('9th Grade Access', 'AVID 9', 'AVID 10', 'AVID 11',
                    'AVID 12', 'Strategies for Success 9-10',
                    'Academic Skills Tutorial', 'ELD Access')
    not_access_names = access_names[1:]

    def load(self, path):
        '''Load an excel file on path.'''
        raise NotImplementedError
        
    def return_sheets(self) -> list:
        '''Return a list of sheets in the excel file.'''
        raise NotImplementedError

    def generate_full_report(self, password=None):
        raise NotImplementedError
    
    def teacher_reports(self,
                        pdf=False,
                        excel=False,
                        password=None,
                        email=False):
        raise NotImplementedError
    
    def counseling_reports(self):
        raise NotImplementedError

    def community_reports(self):
        raise NotImplementedError

    def access_reports(self, password):
        raise NotImplementedError

    def load_sheet(self, sheet: str, semester=1, quarter=1):
        '''Load the specified sheet into a dataframe.
        Remove discipline data if present'''
        raise NotImplementedError
    
    def fresh_template_sheet(self):
        raise NotImplementedError
        
    def load_access(self):
        raise NotImplementedError
    
    def load_counselors(self):
        raise NotImplementedError
    
    def load_partners(self):
        raise NotImplementedError

    def is_access(self, proposed_name: str) -> bool:
        '''This function is needed because RR Teacher is Last, First M.,
        while course listings are Last, First with no middle initial.
        '''
        raise NotImplementedError
    
    def is_teacher(self, proposed_name: str) -> bool:
        '''Returns if teacher is in list'''
        raise NotImplementedError
    
    def is_name(self, name1: str, name2: str) -> bool:
        '''
        Returns True if the names are the same,
        mostly a check for middle initials.
        '''
        raise NotImplementedError

    def copy_cell(self, cell, new_cell):
        raise NotImplementedError


def populate_student(row, full_data, week):
    '''A threaded function to attempt to speed up the slower part of ReportEngineV2.load_sheet()
    The student creation section of that function is slow, especally connecting it to classes.'''
    session = sql.Session()
    last_name, partition, first_name = row['Student Name'].partition(', ')
    #print(last_name, first_name, row['Student ID'], row['Gender'])
    # FROM get_or_create, what is going wrong?

    '''instance = session.query(sql.Student).filter_by(last_name=last_name,
                    first_name=first_name,
                    id=row['Student ID'],
                    gender=row['Gender']).first()
    
    if instance:
        print("Instance:", row['Student ID'], instance, session)
        return instance
    else:
        instance = sql.Student(last_name=last_name,
                    first_name=first_name,
                    id=row['Student ID'],
                    gender=row['Gender'])
        print("New Instance:", row['Student ID'], instance, session)
        session.add(instance)
        try:
            session.commit()
        except Exception as e:
            print("E:", session, last_name, first_name, row['Student ID'], row['Gender'], ':', e, )
            import os
            os._exit(1)
 
    student = instance'''
    ### Resume regular code
    student = sql.get_or_create(session, sql.Student, last_name=last_name,
                    first_name=first_name,
                    id=row['Student ID'],
                    gender=row['Gender'])

    if row['Language to Home']:
        student.language_to_home = row['Language to Home']
    message.set_status.emit(f'Imported {student}')

    try:
        # Process the school year of the 9th grade year
        current_grade = row['Grade']
        if current_grade == '08H':
            current_grade = 8
        elif current_grade == '07H':
            current_grade = 7
        ninth_year = current_school_year - (int(current_grade) - 9)
        student.nineth_grade_year = ninth_year
    except ValueError:
        print("Error importing", last_name, first_name, row['Student ID'], row['Grade'])
    student.ELL = row['ELL']
    if type(row['504']) is str:
        student.five_oh_four = distutils.util.strtobool(row['504'])
    student.diploma = row['Diploma Type']
    student.sped = distutils.util.strtobool(row['SPED'])
    student.race = row['Race']
    session.add(student)

    gpa = sql.get_or_create(session, sql.GPA, year=current_school_year, student_id=student.id, week=week)
    gpa.quarter1_gpa = row['S1 Q1 GPA']
    gpa.quarter2_gpa = row['S1 Q2 GPA']
    gpa.quarter3_gpa = row['S2 Q3 GPA']
    gpa.quarter4_gpa = row['S2 Q4 GPA']
    attendance = sql.get_or_create(session, sql.Attendance, year=current_school_year, student_id=student.id)
    attendance.sept = row['SEP Att %']
    attendance.octo = row['OCT Att %']
    attendance.nov = row['NOV Att %']
    attendance.dec = row['DEC Att %']
    attendance.jan = row['JAN Att %']
    attendance.feb = row['FEB Att %']
    attendance.mar = row['MAR Att %']
    attendance.apr = row['APR Att %']
    attendance.may = row['MAY Att %']
    attendance.jun = row['JUN Att %']
    attendance.ytd_attn = row['YTD ATT %']
    incident = sql.Incident(student_id=student.id,
                    week=week,
                    year=current_school_year,
                    incident_total=row['Incident Total'],
                    iss_total=row['ISS Total'],
                    iss_days=row['ISS Days'],
                    oss_total=row['OSS Total'],
                    oss_days=row['OSS Days'],
                    exp_total=row['Exp Total'])
    session.add(incident)

    # Attach classes to student
    for semester in SEMESTERS:
        for period in PERIODS:
            if f'P{period} S{semester} Course' in full_data:
                if type(row[f'P{period} S{semester} Course']) is str:
                    last_name, first_name, teacher_middle = sql.split_name(row[f'P{period} S{semester} Teacher'])
                    course_name = row[f'P{period} S{semester} Course'].split('-', 1)[1]
                    course_period = int(row[f'P{period} S{semester} Course'].split('-', 1)[0])

                    teacher = session.query(sql.Staff).filter(sql.Staff.last_name == last_name).filter(sql.Staff.first_name == first_name).first()
                    course = session.query(sql.Course).filter(sql.Course.name == course_name).first()
                    class_instance = sql.get_or_create(session, sql.Class, period=course_period, course_id=course.id, teacher_id=teacher.id, semester=semester)
                    message.set_status.emit(f"Attaching {student} to class {class_instance}")
                    if student not in class_instance.students:
                        class_instance.students.append(student)
                        session.add(class_instance)
                    # Import grades
                    # This is naive for the in memory implementation.
                    # On disk we will need to be more intentional about the week being imported
                    grade_entry = sql.get_or_create(session, sql.GradeEntry, student_id=student.id, class_id=class_instance.id, week=week)
                    if semester == 1:
                        if row[f'P{period} S{semester} Mark1']:
                            grade_entry.quarter1_grade = row[f'P{period} S{semester} Mark1']
                        if row[f'P{period} S{semester} Mark2']:
                            grade_entry.quarter2_grade = row[f'P{period} S{semester} Mark2']
                        if row[f'P{period} S{semester} Abs1']:
                            grade_entry.quarter1_abs = row[f'P{period} S{semester} Abs1']
                        if row[f'P{period} S{semester} Abs2']:
                            grade_entry.quarter2_abs = row[f'P{period} S{semester} Abs2']
                    
                    if semester == 2:
                        if row[f'P{period} S{semester} Mark3']:
                            grade_entry.quarter3_grade = row[f'P{period} S{semester} Mark3']
                        if row[f'P{period} S{semester} Mark4']:
                            grade_entry.quarter4_grade = row[f'P{period} S{semester} Mark4']
                        if row[f'P{period} S{semester} Abs3']:
                            grade_entry.quarter3_abs = row[f'P{period} S{semester} Abs3']
                        if row[f'P{period} S{semester} Abs4']:
                            grade_entry.quarter4_abs = row[f'P{period} S{semester} Abs4']
                    session.add(grade_entry)
    session.commit()
    sql.Session.remove()
    message.tick_main_bar.emit()


class ReportEngineV2(ReportEngineAPI):
    '''API for the sql database of the raider report.'''

    def load(self, path):
        '''Load an excel file on path.'''
        self.path = path
        self.excel_pandas = pd.ExcelFile(path)
        message.finished_inital_extraction.emit()

    def return_sheets(self) -> list:
        '''Return a list of sheets in the excel file.'''
        return self.excel_pandas.sheet_names
    
    def generate_full_report(self, password=None):
        raise NotImplementedError
    
    def load_sheet(self, sheet: str, semester=1, quarter=1, week=1):
        '''Load the specified sheet into the database.'''
        self.semester = semester
        self.quarter = quarter
        self.sheet_name = sheet
        session = sql.Session()

        message.set_status.emit("Extracting excel spreadsheet.")
        full_data = self.excel_pandas.parse(sheet_name=sheet, header=1)
        
        message.set_status.emit("Database import: Preparing staff list.")
        staff = set(full_data['Counselor'].dropna().unique().tolist())
        
        for semester in SEMESTERS:
            for period in PERIODS:
                if f'P{period} S{semester} Teacher' in full_data:
                    staff.update(full_data[f'P{period} S{semester} Teacher'].dropna().unique().tolist())

        message.set_status.emit("Database import: Preparing subjects list.")
        #subjects = pd.Series(dtype=str)
        subjects = set([])
        for semester in SEMESTERS:
            for period in PERIODS:
                if f'P{period} S{semester} Subject' in full_data:
                    #subjects = subjects.append(full_data[f'P{period} S{semester} Subject'].dropna().unique())
                    for subject in full_data[f'P{period} S{semester} Subject'].dropna().unique():
                        subjects.update(subject)
        #subjects = subjects.dropna().unique().reset_index(drop=True, name='Subject')

        message.set_status.emit("Database import: Preparing courses list.")
        courses = set()
        classes = []
        for semester in SEMESTERS:
            for period in PERIODS:
                if f'P{period} S{semester} Course' in full_data:
                    course_temp = pd.DataFrame(full_data, columns=[f'P{period} S{semester} Course', f'P{period} S{semester} Teacher']).drop_duplicates().dropna()
                    for index, row in course_temp.iterrows():
                        course_name = row[f'P{period} S{semester} Course'].split('-', 1)[1]
                        courses.add(course_name)
                        classes.append(row[f'P{period} S{semester} Course'])

        # Calculate steps for main bar
        count = 0
        count += len(staff)
        count += len(subjects)
        count += len(courses)
        count += len(classes)
        count += len(pd.DataFrame(full_data, columns=['Student_Name', 'sis_number']).drop_duplicates().index)
        count += len(full_data.index)*7
        message.set_main_bar_max.emit(count-1) # GUI Fudge!

        '''# Populate staff list bulk operation
        staff_members = []
        for raw_name in staff:
            last_name, first_name, middle_initial = sql.split_name(raw_name)
            #staff = sql.get_or_create(session, sql.Staff, last_name=last_name, first_name=first_name)
            staff_members.append(sql.Staff(last_name=last_name, first_name=first_name))
            message.tick_main_bar.emit()
            #message.set_status.emit(f'Imported {staff}')
        
        session.bulk_save_objects(staff_members)'''

        # Populate staff list
        for raw_name in staff:
            last_name, first_name, middle_initial = sql.split_name(raw_name)
            staff_member = sql.get_or_create(session, sql.Staff, last_name=last_name, first_name=first_name)
            message.tick_main_bar.emit()
            message.set_status.emit(f'Imported {staff_member}')

        # Populate Subject
        for subject_name in set(subjects):
            subject = sql.get_or_create(session, sql.Subject, name = subject_name)
            message.tick_main_bar.emit()
            message.set_status.emit(f'Imported {subject}')
        session.commit()

        # Populate course
        # http://www.datasciencemadesimple.com/get-unique-values-rows-dataframe-python-pandas/
        for semester in SEMESTERS:
            for period in PERIODS:
                if f'P{period} S{semester} Subject' in full_data:
                    course_frame = pd.DataFrame(full_data, columns=[f'P{period} S{semester} Subject', f'P{period} S{semester} Course']).drop_duplicates()
                    for index, row in course_frame.iterrows():
                        # I think there is a problem with sorting courses with thast little period thing, odd behavior is happening
                        if type(row[f'P{period} S{semester} Course']) is str:
                            course_name = row[f'P{period} S{semester} Course'].split('-', 1)[1]
                            course = sql.get_or_create(session, sql.Course, name=course_name) 
                            subject = session.query(sql.Subject).filter(sql.Subject.name == row[f'P{period} S{semester} Subject']).first()
                            if subject:
                                course.subject = subject
                            message.set_status.emit(f'Imported {course}')
                        message.tick_main_bar.emit()
        session.commit()

        # Populate Classes
        for semester in SEMESTERS:
            for period in PERIODS:
                if f'P{period} S{semester} Course' in full_data:
                    course_frame = pd.DataFrame(full_data, columns=[f'P{period} S{semester} Course', f'P{period} S{semester} Teacher']).drop_duplicates()
                    for index, row in course_frame.iterrows():
                        if type(row[f'P{period} S{semester} Course']) is str:
                            last_name, first_name, teacher_middle = sql.split_name(row[f'P{period} S{semester} Teacher'])
                            course_name = row[f'P{period} S{semester} Course'].split('-', 1)[1]
                            course_period = int(row[f'P{period} S{semester} Course'].split('-', 1)[0])
                            teacher = session.query(sql.Staff).filter(sql.Staff.last_name == last_name).filter(sql.Staff.first_name == first_name).first()
                            course = session.query(sql.Course).filter(sql.Course.name == course_name).first()

                            query = session.query(sql.Class).filter(sql.Class.course_id == course.id).filter(sql.Class.teacher_id == teacher.id).filter(sql.Class.semester == semester).filter(sql.Class.period==course_period)
                            if not session.query(query.exists()).scalar():
                                class_instance = sql.Class(period=course_period, semester=semester)  # class is a protected keywork in python
                                query = session.query(sql.Staff).filter(sql.Staff.last_name == last_name).filter(sql.Staff.first_name == first_name)
                                class_instance.teacher = query.first()
                                query = session.query(sql.Course).filter(sql.Course.name == course_name)
                                class_instance.course = query.first()
                                message.set_status.emit(f'Imported {class_instance}')
                                session.add(class_instance)
                                session.commit()
                        message.tick_main_bar.emit()
        
        # Populate sports

        for sport_name in full_data['Fall Sports'].dropna().unique().tolist():
            sport = sql.get_or_create(session, sql.Sport,
                                      name=sport_name,
                                      season=sql.SportSeason.FALL,
                                      year=current_school_year)
            session.add(sport)
        for sport_name in full_data['Winter Sports'].dropna().unique().tolist():
            sport = sql.get_or_create(session, sql.Sport,
                                      name=sport_name,
                                      season=sql.SportSeason.WINTER,
                                      year=current_school_year)
            session.add(sport)
        for sport_name in full_data['Spring Sports'].dropna().unique().tolist():
            sport = sql.get_or_create(session, sql.Sport,
                                      name=sport_name,
                                      season=sql.SportSeason.SPRING,
                                      year=current_school_year)
            session.add(sport)
        
        # Populate Students
        '''A quick test of bulk creation'''
        student_dataframe = pd.DataFrame(full_data, columns=['Student Name',
                                                             'Student ID',
                                                             'Gender']).drop_duplicates()
        '''student_dataframe.to_excel('test.xlsx')
        students = []
        ids = []
        for index, row in student_dataframe.iterrows():
            if type(row['Student Name']) is not str:
                continue
            last_name, partition, first_name = row['Student Name'].partition(', ')
            students.append(sql.Student(last_name=last_name,
                    first_name=first_name,
                    id=row['Student ID'],
                    gender=row['Gender']))
            if row['Student ID'] in ids:
                print("DANGER WITH" , row['Student ID'], row['Student Name'])
            ids.append(row['Student ID'])
            session.bulk_save_objects(students)'''


        for index, row in full_data.iterrows():
            if type(row['Student Name']) is not str:
                continue
            #Single Threaded
            populate_student(row, full_data, week)
            #Multithreaded
            #worker = Worker(populate_student, row=row, full_data=full_data, week=week)
            #threadpool.start(worker)
            
        session.commit()
        sql.Session.remove()
        message.tick_main_bar.emit()
        message.super_finished_initial_extraction.emit()
        message.set_status.emit("Import complete")
    
    def convert_report(self, output_filename, week):
        message.set_status.emit("Initializing workbook for conversion.")
        now = datetime.datetime.now()
        current_school_year = now.year
        if now.month <= 8:
            current_school_year -= 1
        session = sql.Session()
        # Calculate steps for progress bar.
        count = 0
        count += session.query(sql.Student).order_by(sql.Student.last_name.asc()).count()
        message.set_convert_bar_max.emit(count)
        message.set_status.emit("Report converter: Creating Header")
        workbook = openpyxl.Workbook(write_only=True)
        worksheet = workbook.create_sheet()
        worksheet.column_dimensions['A'].width = 30
        worksheet.column_dimensions['C'].width = 5
        worksheet.column_dimensions['D'].width = 3
        worksheet.column_dimensions['G'].width = 5
        worksheet.column_dimensions['H'].width = 4
        worksheet.column_dimensions['K'].width = 30
        worksheet.column_dimensions['L'].width = 30
        worksheet.column_dimensions['M'].width = 30
        worksheet.column_dimensions['N'].width = 12
        worksheet.column_dimensions['O'].width = 12
        worksheet.column_dimensions['P'].width = 12
        worksheet.column_dimensions['Q'].width = 12
        worksheet.column_dimensions['R'].width = 12
        worksheet.column_dimensions['S'].width = 20
        # Set up columns
        row = []
        row.append(WriteOnlyCell(worksheet, value='Name'))
        row.append(WriteOnlyCell(worksheet, value='Gender'))
        row.append(WriteOnlyCell(worksheet, value='Grade'))
        row.append(WriteOnlyCell(worksheet, value='ELL'))
        row.append(WriteOnlyCell(worksheet, value='Migrant'))
        row.append(WriteOnlyCell(worksheet, value='504'))
        row.append(WriteOnlyCell(worksheet, value='SPED'))
        row.append(WriteOnlyCell(worksheet, value='TAG'))
        row.append(WriteOnlyCell(worksheet, value='Diploma'))
        row.append(WriteOnlyCell(worksheet, value='SIS Number'))
        row.append(WriteOnlyCell(worksheet, value='Race'))
        for period in PERIODS:
            for semester in (1, 2):
                row.append(WriteOnlyCell(worksheet, value=f'P{period} S{semester} Course'))
                row.append(WriteOnlyCell(worksheet, value=f'P{period} S{semester} Teacher'))
                row.append(WriteOnlyCell(worksheet, value=f'P{period} S{semester} Subject'))
                if semester == 1:
                    row.append(WriteOnlyCell(worksheet, value=f'P{period} S{semester} Mark1'))
                    row.append(WriteOnlyCell(worksheet, value=f'P{period} S{semester} Mark2'))
                    row.append(WriteOnlyCell(worksheet, value=f'P{period} S{semester} Abs1'))
                    row.append(WriteOnlyCell(worksheet, value=f'P{period} S{semester} Abs2'))
                if semester == 2:
                    row.append(WriteOnlyCell(worksheet, value=f'P{period} S{semester} Mark3'))
                    row.append(WriteOnlyCell(worksheet, value=f'P{period} S{semester} Mark4'))
                    row.append(WriteOnlyCell(worksheet, value=f'P{period} S{semester} Abs3'))
                    row.append(WriteOnlyCell(worksheet, value=f'P{period} S{semester} Abs4'))
        row.append(WriteOnlyCell(worksheet, value='GPA 1'))
        row.append(WriteOnlyCell(worksheet, value='GPA 2'))
        row.append(WriteOnlyCell(worksheet, value='GPA 3'))
        row.append(WriteOnlyCell(worksheet, value='GPA 4'))
        row.append(WriteOnlyCell(worksheet, value='SEP Att'))
        row.append(WriteOnlyCell(worksheet, value='OCT Att'))
        row.append(WriteOnlyCell(worksheet, value='NOV Att'))
        row.append(WriteOnlyCell(worksheet, value='DEC Att'))
        row.append(WriteOnlyCell(worksheet, value='JAN Att'))
        row.append(WriteOnlyCell(worksheet, value='FEB Att'))
        row.append(WriteOnlyCell(worksheet, value='MAR Att'))
        row.append(WriteOnlyCell(worksheet, value='APL Att'))
        row.append(WriteOnlyCell(worksheet, value='MAY Att'))
        row.append(WriteOnlyCell(worksheet, value='JUN Att'))
        row.append(WriteOnlyCell(worksheet, value='YTD ATT RT'))
        row.append(WriteOnlyCell(worksheet, value='Incident Total'))
        row.append(WriteOnlyCell(worksheet, value='ISS Total'))
        row.append(WriteOnlyCell(worksheet, value='ISS Days'))
        row.append(WriteOnlyCell(worksheet, value='OSS Total'))
        row.append(WriteOnlyCell(worksheet, value='OSS Days'))
        row.append(WriteOnlyCell(worksheet, value='Exp Total'))
        worksheet.append(row)
        message.tick_convert_bar.emit()

        for student in session.query(sql.Student).order_by(sql.Student.last_name.asc()).all():
            message.set_status.emit(f"Report converter: Processing {student}")
            row = []
            #Name: Last, First
            row.append(WriteOnlyCell(worksheet, value=student.get_name()))
            # Gender
            row.append(WriteOnlyCell(worksheet, value=student.gender))
            # Grade
            if student.nineth_grade_year:
                #row.append(WriteOnlyCell(worksheet, current_school_year - (student.nineth_grade_year + 9)))
                row.append(WriteOnlyCell(worksheet, value=current_school_year - student.nineth_grade_year + 9))
            else:
                row.append(WriteOnlyCell(worksheet, value=''))
            # ELL
            row.append(WriteOnlyCell(worksheet, value=student.ELL))
            # Migrant
            if student.migrant:
                row.append(WriteOnlyCell(worksheet, value='Y'))
            else:
                row.append(WriteOnlyCell(worksheet, value=''))
            # 504
            if student.five_oh_four:
                row.append(WriteOnlyCell(worksheet, value='Y'))
            else:
                row.append(WriteOnlyCell(worksheet, value=''))
            # SPED
            if student.sped:
                row.append(WriteOnlyCell(worksheet, value='Y'))
            else:
                row.append(WriteOnlyCell(worksheet, value='N'))
            # Tag
            if student.tag:
                row.append(WriteOnlyCell(worksheet, value='Y'))
            else:
                row.append(WriteOnlyCell(worksheet, value=''))
            # Diploma
            row.append(WriteOnlyCell(worksheet, value=student.diploma))
            # SIS Number
            row.append(WriteOnlyCell(worksheet, value=student.id))
            # Race
            row.append(WriteOnlyCell(worksheet, value=student.race))

            # Class act
            classes = student.classes
            for period in PERIODS:
                for semester in (1, 2):
                    class_instance = classes.filter(sql.Class.period==period).filter(sql.Class.semester==semester).first()
                    if class_instance:
                        row.append(WriteOnlyCell(worksheet, value=class_instance.get_report_name()))
                        row.append(WriteOnlyCell(worksheet, value=class_instance.teacher.get_name()))
                        row.append(WriteOnlyCell(worksheet, value=class_instance.course.subject.name))
                        entry = session.query(sql.GradeEntry).filter(sql.GradeEntry.student_id == student.id).filter(sql.GradeEntry.class_id == class_instance.id).filter(sql.GradeEntry.week == week).first()
                        if semester == 1:
                            row.append(WriteOnlyCell(worksheet, value=entry.quarter1_grade))
                            row.append(WriteOnlyCell(worksheet, value=entry.quarter2_grade))
                            row.append(WriteOnlyCell(worksheet, value=entry.quarter1_abs))
                            row.append(WriteOnlyCell(worksheet, value=entry.quarter2_abs))
                        if semester == 2:
                            row.append(WriteOnlyCell(worksheet, value=entry.quarter3_grade))
                            row.append(WriteOnlyCell(worksheet, value=entry.quarter4_grade))
                            row.append(WriteOnlyCell(worksheet, value=entry.quarter3_abs))
                            row.append(WriteOnlyCell(worksheet, value=entry.quarter4_abs))
                    else:
                        row.append(WriteOnlyCell(worksheet, value=None))
                        row.append(WriteOnlyCell(worksheet, value=None))
                        row.append(WriteOnlyCell(worksheet, value=None))
                        row.append(WriteOnlyCell(worksheet, value=None))
                        row.append(WriteOnlyCell(worksheet, value=None))
                        row.append(WriteOnlyCell(worksheet, value=None))
                        row.append(WriteOnlyCell(worksheet, value=None))
            # GPA
            gpa = session.query(sql.GPA).filter(sql.GPA.student_id == student.id).filter(sql.GPA.week == week).filter(sql.GPA.year == current_school_year).first()
            row.append(WriteOnlyCell(worksheet, value=gpa.quarter1_gpa))
            row.append(WriteOnlyCell(worksheet, value=gpa.quarter2_gpa))
            row.append(WriteOnlyCell(worksheet, value=gpa.quarter3_gpa))
            row.append(WriteOnlyCell(worksheet, value=gpa.quarter4_gpa))
            # Attn
            attendance = session.query(sql.Attendance).filter(sql.Attendance.student_id == student.id).filter(sql.Attendance.year == current_school_year).first()
            row.append(WriteOnlyCell(worksheet, value=attendance.sept))
            row.append(WriteOnlyCell(worksheet, value=attendance.octo))
            row.append(WriteOnlyCell(worksheet, value=attendance.nov))
            row.append(WriteOnlyCell(worksheet, value=attendance.dec))
            row.append(WriteOnlyCell(worksheet, value=attendance.jan))
            row.append(WriteOnlyCell(worksheet, value=attendance.feb))
            row.append(WriteOnlyCell(worksheet, value=attendance.mar))
            row.append(WriteOnlyCell(worksheet, value=attendance.apr))
            row.append(WriteOnlyCell(worksheet, value=attendance.may))
            row.append(WriteOnlyCell(worksheet, value=attendance.jun))
            row.append(WriteOnlyCell(worksheet, value=attendance.ytd_attn))
            # Naughty
            incident = session.query(sql.Incident).filter(sql.Incident.student_id == student.id).filter(sql.Incident.year == current_school_year).filter(sql.Incident.week == week).first()
            row.append(WriteOnlyCell(worksheet, value=incident.incident_total))
            row.append(WriteOnlyCell(worksheet, value=incident.iss_total))
            row.append(WriteOnlyCell(worksheet, value=incident.iss_days))
            row.append(WriteOnlyCell(worksheet, value=incident.oss_total))
            row.append(WriteOnlyCell(worksheet, value=incident.oss_days))
            row.append(WriteOnlyCell(worksheet, value=incident.exp_total))
            worksheet.append(row)
            message.tick_convert_bar.emit()
        workbook.save(output_filename)
        message.set_status.emit(f"Workbook saved to {output_filename}")
    
    def load_earlywarning_reports(self, file_paths, week, quarter, semester):
        message.set_status.emit("Extracting cvs spreadsheets and preparing progress bar.")
        count = 0
        full_datas = []
        for file_path in file_paths:
            # The unicode escape seems to add random unicode characters to the start of Student_Name
            #full_data = pd.read_csv(self.csv_file_path, encoding="unicode_escape", low_memory=False)
            full_data = pd.read_csv(file_path, low_memory=False)
            count = 0
            count += len(full_data['teacher'].dropna().unique())
            count += len(full_data['Subject'].dropna().unique())
            count += len(pd.DataFrame(full_data, columns=['course', 'Subject']).drop_duplicates().index)
            count += len(pd.DataFrame(full_data, columns=['course', 'teacher', 'Subject', 'mk1', 'mk3']).drop_duplicates().index)
            count += len(pd.DataFrame(full_data, columns=['Student_Name', 'sis_number']).drop_duplicates().index)
            # For gpa processing
            count += len(pd.DataFrame(full_data, columns=['Student_Name', 'sis_number']).drop_duplicates().index)
            count += len(full_data.index)
            full_datas.append(full_data)
        message.set_main_bar_max.emit(count-1) # GUI Fudge!

        message.set_status.emit("Initial scan complete. Now importing early earning reports")

        for full_data in full_datas:
            self.earlywarning_import(full_data, week, quarter, semester)
        message.set_status.emit(f'Calculating GPAs')
        session = sql.Session()
        students = session.query(sql.Student).all()
        print("Number of students:", len(students))
        counter = 0
        for student in students:
            gradepointtotal = 0
            grades = session.query(sql.GradeEntry).where(sql.GradeEntry.student_id == student.id, sql.GradeEntry.week == week).all()
            for grade in grades:
                g = 'f'
                quarter_grade = ''
                if quarter == 1:
                    quarter_grade = grade.quarter1_grade
                elif quarter == 2:
                    quarter_grade = grade.quarter2_grade
                elif quarter == 3:
                    quarter_grade = grade.quarter3_grade
                elif quarter == 4:
                    quarter_grade = grade.quarter4_grade
                if quarter_grade:
                    g = quarter_grade[0].lower()
                if g == 'a':
                    gradepointtotal += 4
                elif g == 'b':
                    gradepointtotal += 3
                elif g == 'c':
                    gradepointtotal += 2
                elif g == 'd':
                    gradepointtotal += 1
            gpa = gradepointtotal / len(grades)

            gpa_object = session.query(sql.GPA).where(sql.GPA.student_id == student.id, sql.GPA.week == week).first()

            if quarter == 1:
                gpa_object.quarter1_gpa = gpa
            elif quarter == 2:
                gpa_object.quarter2_gpa = gpa
            elif quarter == 3:
                gpa_object.quarter3_gpa = gpa
            elif quarter == 4:
                gpa_object.quarter4_gpa = gpa
            
            session.commit()
            message.set_status.emit(f'Calculating GPA: ' + str(counter) + '/' + str(len(students)))
            message.tick_main_bar.emit()
            counter += 1

        message.set_status.emit(f'Early warning import complete')
        message.earlywarning_import_complete.emit()
    
    def earlywarning_import(self, full_data, week, quarter, semester):
        session = sql.Session()
        # Populate staff list
        for raw_name in set(full_data['teacher'].dropna().unique()):
            last_name, first_name, middle_initial = sql.split_name(raw_name)
            staff = sql.get_or_create(session, sql.Staff, last_name=last_name, first_name=first_name)
            message.tick_main_bar.emit()
            message.set_status.emit(f'Imported {staff}')

        # Populate Subject
        for subject_name in set(full_data['Subject'].dropna().unique()):
            subject = sql.get_or_create(session, sql.Subject, name = subject_name)
            message.tick_main_bar.emit()
            message.set_status.emit(f'Imported {subject}')
        session.commit()

        # TODO: Use panda's sql interface to import dataframe directly into sql tables. This should improve speeds significantly.
        # Populate course
        # http://www.datasciencemadesimple.com/get-unique-values-rows-dataframe-python-pandas/
        course_frame = pd.DataFrame(full_data, columns=['course', 'Subject']).drop_duplicates()
        for index, row in course_frame.iterrows():
            # I think there is a problem with sorting courses witht hast little period thing, odd behavior is happening
            if type(row['course']) is str:
                course_name = row['course'].split('-', 1)[1]
                course = sql.get_or_create(session, sql.Course, name=course_name) 
                subject = session.query(sql.Subject).filter(sql.Subject.name == row['Subject']).first()
                if subject:
                    course.subject = subject
                message.set_status.emit(f'Imported {course}')
                session.commit()
            message.tick_main_bar.emit()

        # Populate Classes
        course_frame = pd.DataFrame(full_data, columns=['course', 'teacher', 'Subject', 'mk1', 'mk3']).drop_duplicates()
        for index, row in course_frame.iterrows():
            if type(row['course']) is str:
                last_name, first_name, teacher_middle = sql.split_name(row['teacher'])
                course_name = row['course'].split('-', 1)[1]
                course_period = int(row['course'].split('-', 1)[0])
                teacher = session.query(sql.Staff).filter(sql.Staff.last_name == last_name).filter(sql.Staff.first_name == first_name).first()
                course = session.query(sql.Course).filter(sql.Course.name == course_name).first()
                semester = 1
                if type(row['mk3']) == str:
                    semester = 2

                query = session.query(sql.Class).filter(sql.Class.course_id == course.id).filter(sql.Class.teacher_id == teacher.id).filter(sql.Class.semester == semester).filter(sql.Class.period==course_period)
                
                if not session.query(query.exists()).scalar():
                    class_instance = sql.Class(period=course_period, semester=semester)  # class is a protected keywork in python
                    query = session.query(sql.Staff).filter(sql.Staff.last_name == last_name).filter(sql.Staff.first_name == first_name)
                    class_instance.teacher = query.first()
                    query = session.query(sql.Course).filter(sql.Course.name == course_name)
                    class_instance.course = query.first()
                    message.set_status.emit(f'Imported {class_instance}')
                    session.add(class_instance)
                    session.commit()
            message.tick_main_bar.emit()

        # Populate student
        student_frame = pd.DataFrame(full_data, columns=['Student_Name',
                                                        'sis_number', 
                                                        'stu_gender', 
                                                        'stu_grade', 
                                                        'stu_ELL', 
                                                        'stu_migrant', 
                                                        'stu_504', 
                                                        'Stu_sped', 
                                                        'stu_tag', 
                                                        'stu_diploma', 
                                                        'stu_race_desc',
                                                        'gpa1',
                                                        'gpa2',
                                                        'gpa3',
                                                        'gpa4',
                                                        'SEP_Att',
                                                        'OCT_Att',
                                                        'NOV_Att',
                                                        'DEC_Att',
                                                        'JAN_Att',
                                                        'FEB_Att',
                                                        'MAR_Att',
                                                        'APR_Att',
                                                        'MAY_Att',
                                                        'JUN_Att',
                                                        'YTD_ATT_RT',
                                                        'Incident_total',
                                                        'ISS_Total',
                                                        'ISS_Days',
                                                        'OSS_Total',
                                                        'OSS_Days',
                                                        'Exp_total']).drop_duplicates()
        for index, row in student_frame.iterrows():
            #last_name, first_name = row['Student Name'].split(', ')
            if type(row['Student_Name']) is not str:
                continue
            last_name, partition, first_name = row['Student_Name'].partition(', ')
            student = sql.get_or_create(session, sql.Student, last_name=last_name,
                            first_name=first_name,
                            id=row['sis_number'],
                            gender=row['stu_gender'])
            message.set_status.emit(f'Imported {student}')
            try:
                # Process the school year of the 9th grade year
                now = datetime.datetime.now()
                current_school_year = now.year
                if now.month <= 8:
                    current_school_year -= 1
                current_grade = row['stu_grade']
                if current_grade == '08H':
                    current_grade = 8
                elif current_grade == '07H':
                    current_grade = 7
                ninth_year = current_school_year - (int(current_grade) - 9)
                student.nineth_grade_year = ninth_year
            except ValueError:
                print("Error importing", last_name, first_name, row['sis_number'], row['stu_grade'])
            student.ELL = row['stu_ELL']
            if type(row['stu_migrant']) is str:
                student.migrant = distutils.util.strtobool(row['stu_migrant'])
            if type(row['stu_504']) is str:
                student.five_oh_four = distutils.util.strtobool(row['stu_504'])
            student.diploma = row['stu_diploma']
            student.sped = distutils.util.strtobool(row['Stu_sped'])
            if type(row['stu_tag']) is str:
                student.tag = distutils.util.strtobool(row['stu_tag'])
            student.race = row['stu_race_desc']
            session.add(student)
            gpa = sql.get_or_create(session, sql.GPA, year=current_school_year, student_id=student.id, week=week)
            gpa.quarter1_gpa = row['gpa1']
            gpa.quarter2_gpa = row['gpa2']
            gpa.quarter3_gpa = row['gpa3']
            gpa.quarter4_gpa = row['gpa4']
            attendance = sql.get_or_create(session, sql.Attendance, year=current_school_year, student_id=student.id)
            attendance.sept = row['SEP_Att']
            attendance.octo = row['OCT_Att']
            attendance.nov = row['NOV_Att']
            attendance.dec = row['DEC_Att']
            attendance.jan = row['JAN_Att']
            attendance.feb = row['FEB_Att']
            attendance.mar = row['MAR_Att']
            attendance.apr = row['APR_Att']
            attendance.may = row['MAY_Att']
            attendance.jun = row['JUN_Att']
            attendance.ytd_attn = row['YTD_ATT_RT']
            incident = sql.Incident(student_id=student.id,
                            week=week,
                            year=current_school_year,
                            incident_total=row['Incident_total'],
                            iss_total=row['ISS_Total'],
                            iss_days=row['ISS_Days'],
                            oss_total=row['OSS_Total'],
                            oss_days=row['OSS_Days'],
                            exp_total=row['Exp_total'])
            session.add(incident)
            message.tick_main_bar.emit()
        session.commit()

        # Attach classes to student
        for index, row in full_data.iterrows():
            last_name, first_name, teacher_middle = sql.split_name(row['teacher'])
            course_name = row['course'].split('-', 1)[1]
            course_period = int(row['course'].split('-', 1)[0])
            teacher = session.query(sql.Staff).filter(sql.Staff.last_name == last_name).filter(sql.Staff.first_name == first_name).first()
            course = session.query(sql.Course).filter(sql.Course.name == course_name).first()

            semester = 1
            if type(row['mk3']) == str:
                semester = 2

            class_instance = sql.get_or_create(session, sql.Class, period=course_period, course_id=course.id, teacher_id=teacher.id, semester=semester)  # class is a protected keyword in python
            student = sql.get_or_create(session, sql.Student, id=row['sis_number'])
            message.set_status.emit(f"Attaching {student} to {class_instance}")
            if student not in class_instance.students:
                class_instance.students.append(student)
            # Import grades
            # This is naive for the in memory implementation.
            # On disk we will need to be more intentional about the week being imported
            grade_entry = sql.get_or_create(session, sql.GradeEntry, student_id=student.id, class_id=class_instance.id, week=week)
            if row['mk1']:
                grade_entry.quarter1_grade = row['mk1']
            if row['mk2']:
                grade_entry.quarter2_grade = row['mk2']
            if row['mk3']:
                grade_entry.quarter3_grade = row['mk3']
            if row['mk4']:
                grade_entry.quarter4_grade = row['mk4']
            if row['abs1']:
                grade_entry.quarter1_abs = row['abs1']
            if row['abs2']:
                grade_entry.quarter2_abs = row['abs2']
            if row['abs3']:
                grade_entry.quarter3_abs = row['abs3']
            if row['abs4']:
                grade_entry.quarter4_abs = row['abs4']
            session.add(grade_entry)
            message.tick_main_bar.emit()
        session.commit()
        sql.Session.remove()
        message.tick_main_bar.emit()

    
class ReportEngineV1(ReportEngineAPI):
    '''This is a classified (pun!) version of the command line version of the raider report.
    This is to create a semblence of an api for V2 and V3 of the engine which is sqlite based.

    Version 1 is VERY inefficent with some of the operations. This is the sake for coding clarity for me.
    My hope is version 2 and 3 (memory and on disk) will sacrifice some initial load time to populate the database with faster operations.'''

    def __init__(self):
        self.full_data = None
        self.access_teacher_names = set([])
        self.teacher_names = set([])
        self.counselor_names = set([])
        self.community_partners = set([])
        self.workbook_stream = io.BytesIO()
        self.workbook_template_stream = io.BytesIO()
        self.headers = 1
    
    def get_course_format(self, period, semester) -> str:
        return f'P{period} S{semester} Course'
    
    def get_teacher_format(self, period, semester) -> str:
        return f'P{period} S{semester} Teacher'
    
    def get_gradezone_format(self, quarter, semester) -> str:
        return f'Sem {semester} Quarter {quarter} Grades Zone'
    
    def get_mark_format(self, period, semester, quarter) -> str:
        # ????
        return f"P{period} S{semester} Mark Q{quarter}"

    def load(self, path):
        '''Load an excel file on path.'''
        self.path = path
        self.excel_pandas = pd.ExcelFile(path)
        message.finished_inital_extraction.emit()
        
    def return_sheets(self) -> list:
        '''Return a list of sheets in the excel file.'''
        return self.excel_pandas.sheet_names
    
    def teacher_reports(self, pdf=False, excel=False, password=None, email=False):
        if not os.path.exists('output/teacher_yellow_report'):
            os.makedirs('output/teacher_yellow_report')
        teacher_workbook, sheet = self.fresh_sheet()
        message.set_teacher_bar_max.emit(int(len(self.teacher_names) * len(PERIODS) - 1 + len(self.teacher_names)*2 + (sheet.max_row - 3)))
        styles = getSampleStyleSheet()
        teacher_workbooks = {}

        if excel:
            ### Temp all teacher report file
            # {teacher_name: [workbook, worksheet, row_counter]}
            for teacher_name in self.teacher_names:
                message.set_status.emit("Creating individual teacher reports: " + teacher_name)
                teacher_workbook, teacher_sheet = self.fresh_template_sheet()
                message.set_status.emit("Teacher reports: Trimming book for: " + teacher_name)
                teacher_sheet.title = teacher_name
                teacher_workbooks[teacher_name] = [teacher_workbook, teacher_sheet, 3]
                message.tick_teacher_bar.emit()
            
            period_name_columns = {} # Columns of the name of teachers by periods
            for p in PERIODS:
                period_name_columns[p] = None
            for row in sheet.iter_rows(min_row=2, max_row=2):
                for period in PERIODS:
                    for cell in row:
                        if self.is_name(cell.value, self.get_teacher_format(period, self.semester)):
                            period_name_columns[period] = cell.column
                            break
            
            ### Temp all teacher report file
            # {teacher_name: [workbook, worksheet, row_counter]}
            n = 0
            for row in sheet.iter_rows(min_row=3):
                message.set_status.emit("Creating individual teacher reports: " + str(n) + '/' + str(sheet.max_row))
                for period in PERIODS: 
                    if period_name_columns[period] is not None:
                        teacher_name = row[period_name_columns[period] - 1].value # -1 due to index of 0 in python vs index of 1 in openpyexcel
                        if self.is_teacher(teacher_name):
                            t_sheet = teacher_workbooks[teacher_name][1]
                            t_row = teacher_workbooks[teacher_name][2]
                            for cell in row:
                                new_cell = t_sheet.cell(row=t_row, column=cell.col_idx, value=cell.value)
                                new_cell = self.copy_cell(cell, new_cell)
                            teacher_workbooks[teacher_name][2] += 1
                message.tick_teacher_bar.emit()
                n += 1

            #pythoncom.CoInitialize()
            for teacher_name in self.teacher_names:
                message.set_status.emit("Saving individual teacher reports: " + teacher_name)
                filename = f'output/teacher_yellow_report/{teacher_name}.xlsx'
                filename_password = f'output/teacher_yellow_report/{teacher_name}.password.xlsx'
                teacher_workbooks[teacher_name][0].save(filename)
                teacher_workbooks[teacher_name][0].close()
                #if password:
                #    cwd = os.getcwd()
                #    excel_file_path = os.path.abspath(os.path.join(cwd, filename))
                #    excel_file_path_password = os.path.abspath(os.path.join(cwd, filename_password))
                #    xcl = win32com.client.Dispatch('Excel.Application')
                #    wb = xcl.Workbooks.Open(excel_file_path, False, False, None)
                #    wb.SaveAs(excel_file_path_password, None, password)
                #    xcl.Quit()
                message.tick_teacher_bar.emit()
        teacher_workbooks = {}

        if pdf:
            for teacher_name in self.teacher_names:
                message.set_status.emit("Creating individual teacher pdf: " + teacher_name)
                formatted_time = time.ctime()
                story = []
                c = SimpleDocTemplate('output/teacher_yellow_report/' + teacher_name + '.pdf', pagesize=letter)
                ptext = "CONFIDENTIAL: FERPA!"
                story.append(Paragraph(ptext, styles["Heading1"]))
                ptext = "Failing student report for " + str(teacher_name) + ". Generated on: " + str(formatted_time)
                story.append(Paragraph(ptext, styles["Normal"]))
                ptext = "I'm a bot. Beep Boop. The following students are currently not passing your class. Yellow zone students are failing 2-3 classes. Red zone students are failing 4-7 classes."
                story.append(Paragraph(ptext, styles["Normal"]))
                table_story = []
                for period in PERIODS:
                    if self.get_teacher_format(period, self.semester) in self.full_data:
                        class_set = self.full_data[self.full_data[self.get_teacher_format(period, self.semester)] == teacher_name]
                        class_set_red = class_set[class_set[self.get_gradezone_format(self.quarter, self.semester)] == 'Red']
                        class_set_yellow = class_set[class_set[self.get_gradezone_format(self.quarter, self.semester)] == 'Yellow']
                        class_set_green = class_set[class_set[self.get_gradezone_format(self.quarter, self.semester)] == 'Green']
                        green_inclass = class_set_green[class_set_green[self.get_mark_format(period, self.semester, self.quarter)] == 'F']
                        green_students = list(green_inclass['Student Name'].values)
                        yellow_inclass = class_set_yellow[class_set_yellow[self.get_mark_format(period, self.semester, self.quarter)] == 'F']
                        yellow_students = list(yellow_inclass['Student Name'].values)
                        red_inclass = class_set_red[class_set_red[self.get_mark_format(period, self.semester, self.quarter)] == 'F']
                        red_students = list(red_inclass['Student Name'].values)
                        if yellow_students or red_students or green_students:
                            ptext = 'Period ' + str(period) + ':'
                            table_story.append([ptext, ''])
                            for student in green_students:
                                ptext = student
                                table_story.append([ptext, "Green"])
                            for student in yellow_students:
                                ptext = student
                                table_story.append([ptext, "Yellow"])
                            for student in red_students:
                                ptext = student
                                table_story.append([ptext, "Red"])
                    message.tick_teacher_bar.emit()
                if table_story:
                    table = Table(table_story)
                    table.setStyle([('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                            ('BOX', (0,0), (-1,-1), 0.25, colors.black)])
                    story.append(table)
                c.build(story)
                
                document = PyPDF2.PdfFileReader('output/teacher_yellow_report/' + teacher_name + '.pdf')
                document_writer = PyPDF2.PdfFileWriter()
                for page in range(document.getNumPages()):
                    document_writer.addPage(document.getPage(page))
                if password:
                    document_writer.encrypt(user_pwd=password, owner_pwd=None, use_128bit=True)
                with open('output/teacher_yellow_report/' + teacher_name + '.pdf', 'wb') as file_handler:
                    document_writer.write(file_handler)            
        
        message.set_status.emit('Teacher report: DONE!')

    def counseling_reports(self):
        message.set_status.emit("Generating counseler reports...")
        if not os.path.exists('output/counselor_report'):
            os.makedirs('output/counselor_report')
        counselor_name_column = 1
        counselor_workbook, sheet = self.fresh_sheet()
        message.set_status.emit("Counselor reports: Processing Excel Matrix...")

        # {counselor_name: [workbook, worksheet, row_counter]}
        counselor_workbooks = {}

        for row in sheet.iter_rows(min_row=2, max_row=2):
            for cell in row:
                if cell.value == 'Counselor':
                    counselor_name_column = cell.column
        
        message.set_counselor_bar_max.emit(int(len(self.counselor_names) * 2 + (sheet.max_row - 3)))
        
        for counselor_name in self.counselor_names:
            message.set_status.emit("Creating individual counseler reports: " + counselor_name)
            counselor_workbook, counselor_sheet = self.fresh_template_sheet()
            message.set_status.emit("Counselor reports: Trimming book for: " + counselor_name)
            counselor_sheet.title = counselor_name
            #counselor_sheet.delete_rows(3, counselor_sheet.max_row)
            #from openpyxl.workbook.protection import WorkbookProtection
            #counselor_workbook.security = WorkbookProtection()
            #counselor_workbook.security.set_workbook_password('test')
            #counselor_workbook.security.lockStructure = True
            counselor_workbooks[counselor_name] = [counselor_workbook, counselor_sheet, 3]
            message.tick_counselor_bar.emit()
    
        n = 0
        for row in sheet.iter_rows(min_row=3):
            message.set_status.emit("Creating individual counseler reports: " + str(n) + '/' + str(sheet.max_row))
            counselor_name = row[counselor_name_column-1].value # -1 due to index of 0 in python vs index of 1 in openpyexcel
            if counselor_name not in self.counselor_names:
                message.tick_counselor_bar.emit()
                n += 1
                continue
            c_sheet = counselor_workbooks[counselor_name][1]
            c_row = counselor_workbooks[counselor_name][2]
            for cell in row:
                new_cell = c_sheet.cell(row=c_row, column=cell.col_idx, value=cell.value)
                new_cell = self.copy_cell(cell, new_cell)
            counselor_workbooks[counselor_name][2] += 1
            message.tick_counselor_bar.emit()
            n += 1

        for counselor_name in self.counselor_names:
            message.set_status.emit("Saving individual counseler reports: " + counselor_name)
            #counselor_workbooks[counselor_name][0].security.workbookPassword = 'test'
            counselor_workbooks[counselor_name][0].save('output/counselor_report/' + str(counselor_name) + '.xlsx')
            counselor_workbooks[counselor_name][0].close()
            message.tick_counselor_bar.emit()
        
        counselor_workbooks = None
        message.set_status.emit('Counselor report: DONE!')

    def community_reports(self):
        # Create a temp file to store an excel sheet that has only has community partner
        # students. This will greatly speed up run time
        message.set_status.emit("Generating community partner reports.")
        if not os.path.exists('output/community_partners'):
            os.makedirs('output/community_partners')
        partner_name_column = 0
        partner_workbook, sheet = self.fresh_sheet()
        message.set_status.emit("Partner reports: Processing Excel Matrix...")

        for row in sheet.iter_rows(min_row=2, max_row=2):
            for cell in row:
                if cell.value == 'Community Partner Organization':
                    partner_name_column = cell.column
        
        message.set_partner_bar_max.emit(int(sheet.max_row - 2 + len(self.community_partners)))
        # {partner_name: [workbook, worksheet, row_counter]}
        partner_workbooks = {}

        for partner_name in self.community_partners:
            message.set_status.emit("Creating partner reports: " + partner_name)
            partner_workbook, partner_sheet = self.fresh_template_sheet()
            message.set_status.emit("Partner reports: Trimming book for: " + partner_name)
            partner_sheet.title = partner_name
            partner_workbooks[partner_name] = [partner_workbook, partner_sheet, 3]
            message.tick_partner_bar.emit()
        
        n = 0
        for row in sheet.iter_rows(min_row=3):
            message.set_status.emit("Creating partner reports: " + str(n) + '/' + str(sheet.max_row))
            partner_name = row[partner_name_column-1].value # -1 due to index of 0 in python vs index of 1 in openpyexcel
            if partner_name not in self.community_partners:
                message.tick_partner_bar.emit()
                n += 1
                continue
            p_sheet = partner_workbooks[partner_name][1]
            p_row = partner_workbooks[partner_name][2]
            for cell in row:
                new_cell = p_sheet.cell(row=p_row, column=cell.col_idx, value=cell.value)
                new_cell = self.copy_cell(cell, new_cell)
            partner_workbooks[partner_name][2] += 1
            message.tick_partner_bar.emit()
            n += 1

        for partner_name in self.community_partners:
            message.set_status.emit("Saving individual partner reports: " + partner_name)
            partner_workbooks[partner_name][0].save('output/community_partners/' + str(partner_name) + '.xlsx')
            partner_workbooks[partner_name][0].close()
            message.tick_partner_bar.emit()
        
        partner_workbooks = None
        message.set_status.emit('Partner report: DONE!')

    def access_reports(self, password):
        if not os.path.exists('output/access_yellow_report'):
            os.makedirs('output/access_yellow_report')
        access_workbook, sheet = self.fresh_sheet()
        message.set_status.emit("Generating access reports.")

        raw_rows = sheet.max_row - 2
        access_rows = 0
        for x in self.full_data['RR Teacher']:
            if self.is_access(x):
                access_rows += 1
        
        message.set_access_bar_max.emit(int(len(tuple(sheet.columns)) + len(self.access_teacher_names)*2 + raw_rows-2))

        access_teacher_column = 1
        message.set_status.emit("Access reports: Processing Excel Matrix")

        for row in sheet.iter_rows(min_row=2, max_row=2):
            for cell in row:
                if cell.value == 'RR Teacher' or cell.value == 'Access / AVID Teacher':
                    access_teacher_column = cell.column
                message.tick_access_bar.emit()

        print("Debug: Access column:", access_teacher_column)
        message.set_status.emit("Access reports: Initial Optimization")

        # {partner_name: [workbook, worksheet, row_counter]}
        access_workbooks = {}

        for name in self.access_teacher_names:
            message.set_status.emit("Creating access reports: " + name)
            access_workbook, access_sheet = self.fresh_template_sheet()
            message.set_status.emit("Access reports: Trimming book for: " + name)
            access_sheet.title = name
            access_workbooks[name] = [access_workbook, access_sheet, 3]
            message.tick_access_bar.emit()

        message.set_status.emit("Access reports phase 1/2: Generating Individual Access/AVID Reports")
        n = 0
        for row in sheet.iter_rows(min_row=3):
            message.set_status.emit("Creating access reports: " + str(n) + '/' + str(sheet.max_row))
            access_name = row[access_teacher_column-1].value # -1 due to index of 0 in python vs index of 1 in openpyexcel
            if not self.is_access(access_name):
                message.tick_access_bar.emit()
                n += 1
                #print("No dice:", access_name, self.access_teacher_names)
                continue
            # Need to migrade to a better system for this middle initial problem
            if access_name[-1] == '.':
                access_name = access_name[:-3]
            p_sheet = access_workbooks[access_name][1]
            p_row = access_workbooks[access_name][2]
            for cell in row:
                new_cell = p_sheet.cell(row=p_row, column=cell.col_idx, value=cell.value)
                new_cell = self.copy_cell(cell, new_cell)
            access_workbooks[access_name][2] += 1
            message.tick_access_bar.emit()
            n += 1

        styles=getSampleStyleSheet()
        for teacher_name in self.access_teacher_names:
            message.set_status.emit("Access reports phase 2/2: Generating report: " + teacher_name)
            # TODO: Iterate the rows of the prunned access_workbook just above
            # using for index, row in full_data.iterrows(): near the top. This way I
            # wont have to deal with the middle initial sadness in RR Teacher.
            access_workbooks[teacher_name][0].save('output/access_yellow_report/' + str(teacher_name) + '.xlsx')
            access_workbooks[teacher_name][0].close()

            formatted_time = time.ctime()
            story = []
            c = SimpleDocTemplate('output/access_yellow_report/' + teacher_name + '.pdf', pagesize=letter)
            width, height = letter
            ptext = "CONFIDENTIAL: FERPA!"
            story.append(Paragraph(ptext, styles["Heading1"]))
            ptext = "Failing student report for " + str(teacher_name) + ". Generated on: " + str(formatted_time)
            story.append(Paragraph(ptext, styles["Normal"]))
            ptext = "I'm a bot. Beep Boop. The following students in your access classes are failing 2 or more classes. Yellow zone students are failing 2-3 classes. Red zone students are failing 4-7 classes."
            story.append(Paragraph(ptext, styles["Normal"]))
            table_story = []

            teacher_set = pd.read_excel('output/access_yellow_report/' + str(teacher_name) + '.xlsx', header=1)
            for period in PERIODS:
                class_set = teacher_set[teacher_set['RR Period'] == period]
                if not class_set.empty:
                    class_set_yellow = class_set[class_set[self.get_gradezone_format(self.quarter, self.semester)] == 'Yellow']
                    class_set_red = class_set[class_set[self.get_gradezone_format(self.quarter, self.semester)] == 'Red']
                    if not class_set_yellow.empty or not class_set_red.empty:
                        ptext = 'Period ' + str(period) + ':'
                        table_story.append([ptext, '', '','','','',''])
                        table_story.append(['Student', 'ID', 'Class', 'Teacher', 'Period', 'Grade', 'Zone'])
                        for index, row in class_set_yellow.iterrows():
                            for class_period in PERIODS:
                                if self.get_mark_format(class_period, self.semester, self.quarter) in row:
                                    if row[self.get_mark_format(class_period, self.semester, self.quarter)] == 'F':
                                        student_story = []
                                        student_story.append(row['Student Name'])
                                        student_story.append(str(row['Student ID']))
                                        student_story.append(row[self.get_course_format(class_period, self.semester)])
                                        student_story.append(row[self.get_teacher_format(class_period, self.semester)])
                                        student_story.append(str(class_period))
                                        student_story.append(row[self.get_mark_format(class_period, self.semester, self.quarter)])
                                        student_story.append("Yellow")
                                        table_story.append(student_story)
                        for index, row in class_set_red.iterrows():
                            for class_period in PERIODS:
                                if self.get_mark_format(class_period, self.semester, self.quarter) in row:
                                    if row[self.get_mark_format(class_period, self.semester, self.quarter)] == 'F':
                                        student_story = []
                                        student_story.append(row['Student Name'])
                                        student_story.append(str(row['Student ID']))
                                        student_story.append(row[self.get_course_format(class_period, self.semester)])
                                        student_story.append(row[self.get_teacher_format(class_period, self.semester)])
                                        student_story.append(str(class_period))
                                        student_story.append(row[self.get_mark_format(class_period, self.semester, self.quarter)])
                                        student_story.append("Red")
                                        table_story.append(student_story)
            if table_story:
                table = Table(table_story)
                table.setStyle([('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                        ('BOX', (0,0), (-1,-1), 0.25, colors.black)])
                story.append(table)
            c.build(story)

            if password:
                document = PyPDF2.PdfReader('output/access_yellow_report/' + teacher_name + '.pdf')
                document_writer = PyPDF2.PdfWriter()
                for page in range(document.getNumPages()):
                    document_writer.addPage(document.getPage(page))
                document_writer.encrypt(user_pwd=password, owner_pwd=None, use_128bit=True)
                with open('output/access_yellow_report/' + teacher_name + '.pdf', 'wb') as file_handler:
                    document_writer.write(file_handler)
            message.tick_access_bar.emit()
        access_workbooks = None
        message.set_status.emit("Access Reports: Completed.")        

    def load_sheet(self, sheet: str, semester=1, quarter=1):
        '''Load the specified sheet into a dataframe. Remove discipline data if present'''
        message.set_status.emit("Extracting excel spreadsheet.")
        message.set_main_bar_max.emit(6)
        self.sheet_name = sheet
        # Converted early warning
        #headers = 0
        # Full report
        #headers = 1
        print("Header:", self.headers)
        self.full_data = self.excel_pandas.parse(sheet_name=sheet, header=self.headers)
        message.set_status.emit("Sheet extracted.")
        message.tick_main_bar.emit()
        self.semester = semester
        self.quarter = quarter
        message.tick_main_bar.emit()

        message.set_status.emit("Loading workbook. This step takes a while for large workbooks.")
        print("Path:", self.path)
        workbook = openpyxl.load_workbook(self.path)
        # Remove unsed sheets
        message.set_status.emit("Running initial spreadsheet optimization.")
        message.tick_main_bar.emit()
        for s in workbook.sheetnames:
            if s != self.sheet_name:
                workbook.remove(workbook[s])
        message.tick_main_bar.emit()
        workbook.save(self.workbook_stream)
        message.set_status.emit("Creating template.")
        message.tick_main_bar.emit()
        self.openpyxl_style = workbook[self.sheet_name].conditional_formatting
        workbook[self.sheet_name].delete_rows(3, workbook[self.sheet_name].max_row)
        ## Test ##
        #workbook.defined_names._cleanup()
        ##      ##
        workbook.save(self.workbook_template_stream)
        if not os.path.exists('output'):
            os.makedirs('output')
        workbook.save("output/template.xlsx")
        workbook.close()
        message.set_status.emit("Populating teacher lists.")
        message.tick_main_bar.emit()
        self.load_staff()
        message.set_status.emit("Spreadsheet extracted.")
        message.tick_main_bar.emit()
        message.super_finished_initial_extraction.emit()
    
    def fresh_sheet(self):
        message.set_status.emit("Generating fresh workbook.")
        workbook = openpyxl.load_workbook(self.workbook_stream)
        # EI = 139
        sheet = workbook[self.sheet_name]
        # TODO: This moved to EL and EL-EQ in WEek 9 2019
        # Blank discipline. Removal fubars the merged cell headers.
        '''if sheet['EI1'].value == 'Discipline':
            for row_number in range(3, sheet.max_row + 1, 1):
                for col in ('EI', 'EJ', 'EK', 'EL', 'EM', 'EN'):
                    sheet[col+str(row_number)] = ''
                    '''
        message.set_status.emit("Fresh workbook generated.")
        return workbook, sheet
    
    def fresh_template_sheet(self):
        message.set_status.emit("Generating fresh workbook.")
        workbook = openpyxl.load_workbook(self.workbook_template_stream)
        # EI = 139
        sheet = workbook[self.sheet_name]
        # TODO: This moved to EL and EL-EQ in WEek 9 2019
        # Blank discipline. Removal fubars the merged cell headers.
        '''if sheet['EI1'].value == 'Discipline':
            for row_number in range(3, sheet.max_row + 1, 1):
                for col in ('EI', 'EJ', 'EK', 'EL', 'EM', 'EN'):
                    sheet[col+str(row_number)] = ''
                    '''
        message.set_status.emit("Fresh workbook generated.")
        return workbook, sheet
    
    def load_staff(self):
        print("Loading staff")
        try:
            self.community_partners = set(self.full_data['Community Partner Organization'].dropna().unique())
        except Exception as e:
            self.community_partners = set()
            print(e)
        try:
            self.counselor_names = set(self.full_data['Counselor'].dropna().unique())
        except Exception as e:
            self.counselor_names = set()
            print(e)
        message.set_status.emit("Sorting Access and AVID Teachers...")
        for index, row in self.full_data.iterrows():
            for period in PERIODS:
                if self.get_teacher_format(period, self.semester) in row:
                    if type(row[self.get_teacher_format(period, self.semester)]) is str:
                        self.teacher_names.add(row[self.get_teacher_format(period, self.semester)])
                        if type(row[self.get_course_format(period, self.semester)]) is str and '9th Grade Access' in row[self.get_course_format(period, self.semester)]:
                            self.access_teacher_names.add(row[self.get_teacher_format(period, self.semester)])
                        if type(row[self.get_course_format(period, self.semester)]) is str and 'AVID 9' in row[self.get_course_format(period, self.semester)]:
                            self.access_teacher_names.add(row[self.get_teacher_format(period, self.semester)])
                        if type(row[self.get_course_format(period, self.semester)]) is str and 'AVID 10' in row[self.get_course_format(period, self.semester)]:
                            self.access_teacher_names.add(row[self.get_teacher_format(period, self.semester)])
                        if type(row[self.get_course_format(period, self.semester)]) is str and 'AVID 11' in row[self.get_course_format(period, self.semester)]:
                            self.access_teacher_names.add(row[self.get_teacher_format(period, self.semester)])
                        if type(row[self.get_course_format(period, self.semester)]) is str and 'AVID 12' in row[self.get_course_format(period, self.semester)]:
                            self.access_teacher_names.add(row[self.get_teacher_format(period, self.semester)])
                        if type(row[self.get_course_format(period, self.semester)]) is str and 'Strategies for Success 9-10' in row[self.get_course_format(period, self.semester)]:
                            self.access_teacher_names.add(row[self.get_teacher_format(period, self.semester)])
                        if type(row[self.get_course_format(period, self.semester)]) is str and 'Academic Skills Tutorial' in row[self.get_course_format(period, self.semester)]:
                            self.access_teacher_names.add(row[self.get_teacher_format(period, self.semester)])
                        if type(row[self.get_course_format(period, self.semester)]) is str and 'ELD Access' in row[self.get_course_format(period, self.semester)]:
                            self.access_teacher_names.add(row[self.get_teacher_format(period, self.semester)])
        print("Debug: Community Partners:", self.community_partners)
        print("Debug: Counselor:", self.counselor_names)
        print("Debug: Access teachers:", self.access_teacher_names)
        print("Debug: Number of teachers:", len(self.teacher_names))

    def is_access(self, proposed_name: str) -> bool:
        '''This function is needed because RR Teacher is Last, First M., while course listings are Last, First with no middle initial.
        '''
        if type(proposed_name) is not str:
            return False
        for teacher in self.access_teacher_names:
            if proposed_name.startswith(teacher) or teacher.startswith(proposed_name):
                return True
        return False
    
    def is_teacher(self, proposed_name: str) -> bool:
        '''Returns if teacher is in list'''
        for teacher in self.teacher_names:
            if self.is_name(teacher, proposed_name):
                return True
        return False
    
    def is_name(self, name1: str, name2: str) -> bool:
        '''
        Returns True if the names are the same, mostly a check for middle initials.
        '''
        if type(name1) is not str or type(name2) is not str:
            return False
        return name1.startswith(name2) or name2.startswith(name1)

    def copy_cell(self, cell, new_cell):
        new_cell.data_type = cell.data_type
        new_cell._value = cell._value
        if cell.has_style:
            new_cell.font = copy(cell.font)
            new_cell.border = copy(cell.border)
            new_cell.fill = copy(cell.fill)
            new_cell.number_format = copy(cell.number_format)
            new_cell.protection = copy(cell.protection)
            new_cell.alignment = copy(cell.alignment)
            #new_cell._style = copy(cell._style)
        if cell.hyperlink:
            new_cell._hyperlink = copy(cell.hyperlink)
        return new_cell


class ReportEngineV1N2020(ReportEngineV1):
    '''Revised for the 2020 update, some changes in format and no community partners'''
    def get_course_format(self, period, semester) -> str:
        return f'S{semester} P{period} Course'
    
    def get_teacher_format(self, period, semester) -> str:
        return f'S{semester} P{period} Teacher'
    
    def get_gradezone_format(self, quarter, semester) -> str:
        return f'S{semester} Q{quarter} Student Marks Zone'
    
    def get_mark_format(self, period, semester, quarter) -> str:
        # ????
        return f"S{semester} P{period} Mark Q{quarter}"
    
    def access_reports(self, password):
        # For 2022 edition books
        if not os.path.exists('output/access_yellow_report'):
            os.makedirs('output/access_yellow_report')
        access_workbook, sheet = self.fresh_sheet()
        message.set_status.emit("Generating access reports.")

        raw_rows = sheet.max_row - 2
        
        message.set_access_bar_max.emit(int(len(tuple(sheet.columns)) + len(self.access_teacher_names)*2 + raw_rows-3))

        access_teacher_column = 1
        message.set_status.emit("Access reports: Processing Excel Matrix")

        for row in sheet.iter_rows(min_row=2, max_row=2):
            for cell in row:
                if cell.value == '9th Grade Access':
                    access_teacher_column = cell.column
                message.tick_access_bar.emit()

        print("Debug: Access column:", access_teacher_column)
        message.set_status.emit("Access reports: Initial Optimization")

        # {partner_name: [workbook, worksheet, row_counter]}
        access_workbooks = {}

        for name in self.access_teacher_names:
            message.set_status.emit("Creating access reports: " + name)
            access_workbook, access_sheet = self.fresh_template_sheet()
            message.set_status.emit("Access reports: Trimming book for: " + name)
            access_sheet.title = name
            access_workbooks[name] = [access_workbook, access_sheet, 3]
            message.tick_access_bar.emit()

        message.set_status.emit("Access reports phase 1/2: Generating Individual Access/AVID Reports")

        # This block is for the non-access reports processing
        period_name_columns = {} # Columns of the name of teachers by periods
        period_course_columns = {}
        for p in PERIODS:
            period_name_columns[p] = None
            period_course_columns[p] = None
        for row in sheet.iter_rows(min_row=2, max_row=2):
            for period in PERIODS:
                for cell in row:
                    if self.is_name(cell.value, self.get_teacher_format(period, self.semester)):
                        period_name_columns[period] = cell.column
                    if cell.value == self.get_course_format(period, self.semester):
                        period_course_columns[period] = cell.column

        n = 0
        for row in sheet.iter_rows(min_row=3):
            message.set_status.emit("Creating access reports: " + str(n) + '/' + str(sheet.max_row))
            # This code works for 9th grade access only
            access_name = row[access_teacher_column-1].value # -1 due to index of 0 in python vs index of 1 in openpyexcel
            # This optimization is useful looking only for 9th grade access.
            #if not self.is_access(access_name):
            #    message.tick_access_bar.emit()
            #    n += 1
            #    #print("No dice:", access_name, self.access_teacher_names)
            #    continue
            # Need to migrade to a better system for this middle initial problem
            if self.is_access(access_name):
                if access_name[-1] == '.':
                    access_name = access_name[:-3]
                p_sheet = access_workbooks[access_name][1]
                p_row = access_workbooks[access_name][2]
                for cell in row:
                    new_cell = p_sheet.cell(row=p_row, column=cell.col_idx, value=cell.value)
                    new_cell = self.copy_cell(cell, new_cell)
                access_workbooks[access_name][2] += 1
                message.tick_access_bar.emit()
                n += 1
                continue

            # This code is for access like classes.
            for period in PERIODS:
                if period_course_columns[period]:
                    if row[period_course_columns[period] - 1].value is not None:
                        course_name = row[period_course_columns[period] - 1].value.split("-")[1] # Peel of the period number
                        if course_name in self.not_access_names:
                            teacher_name = row[period_name_columns[period] - 1].value
                            p_sheet = access_workbooks[teacher_name][1]
                            p_row = access_workbooks[teacher_name][2]
                            for cell in row:
                                new_cell = p_sheet.cell(row=p_row, column=cell.col_idx, value=cell.value)
                                new_cell = self.copy_cell(cell, new_cell)
                            access_workbooks[teacher_name][2] += 1
                            continue


            message.tick_access_bar.emit()
            n += 1

        styles=getSampleStyleSheet()
        for teacher_name in self.access_teacher_names:
            message.set_status.emit("Access reports phase 2/2: Generating report: " + teacher_name)
            # TODO: Iterate the rows of the prunned access_workbook just above
            # using for index, row in full_data.iterrows(): near the top. This way I
            # wont have to deal with the middle initial sadness in RR Teacher.
            access_workbooks[teacher_name][0].save('output/access_yellow_report/' + str(teacher_name) + '.xlsx')
            access_workbooks[teacher_name][0].close()

            """formatted_time = time.ctime()
            story = []
            c = SimpleDocTemplate('output/access_yellow_report/' + teacher_name + '.pdf', pagesize=letter)
            width, height = letter
            ptext = "CONFIDENTIAL: FERPA!"
            story.append(Paragraph(ptext, styles["Heading1"]))
            ptext = "Failing student report for " + str(teacher_name) + ". Generated on: " + str(formatted_time)
            story.append(Paragraph(ptext, styles["Normal"]))
            ptext = "I'm a bot. Beep Boop. The following students in your access classes are failing 2 or more classes. Yellow zone students are failing 2-3 classes. Red zone students are failing 4-7 classes."
            story.append(Paragraph(ptext, styles["Normal"]))
            table_story = []

            teacher_set = pd.read_excel('output/access_yellow_report/' + str(teacher_name) + '.xlsx', header=1)
            for period in PERIODS:
                class_set = teacher_set[teacher_set['RR Period'] == period]
                #print("Debug Class set:", teacher_name, period)
                if not class_set.empty:
                    class_set_yellow = class_set[class_set[self.get_gradezone_format(self.quarter, self.semester)] == 'At-Risk']
                    class_set_red = class_set[class_set[self.get_gradezone_format(self.quarter, self.semester)] == 'Off-Track']
                    #print("Debug yellow:", class_set_yellow)
                    #print("Debug red:", class_set_red)
                    if not class_set_yellow.empty or not class_set_red.empty:
                        ptext = 'Period ' + str(period) + ':'
                        table_story.append([ptext, '', '','','','',''])
                        table_story.append(['Student', 'ID', 'Class', 'Teacher', 'Period', 'Grade', 'Zone'])
                        for index, row in class_set_yellow.iterrows():
                            for class_period in PERIODS:
                                if self.get_mark_format(class_period, self.semester, self.quarter) in row:
                                    if row[self.get_mark_format(class_period, self.semester, self.quarter)] == 'F' or row[self.get_mark_format(class_period, self.semester, self.quarter)] == 'NP':
                                        student_story = []
                                        student_story.append(row['Student Name'])
                                        student_story.append(str(row['Student ID']))
                                        student_story.append(row[self.get_course_format(class_period, self.semester)])
                                        student_story.append(row[self.get_teacher_format(class_period, self.semester)])
                                        student_story.append(str(class_period))
                                        student_story.append(row[self.get_mark_format(class_period, self.semester, self.quarter)])
                                        student_story.append("At-Risk (Yellow)")
                                        table_story.append(student_story)
                        for index, row in class_set_red.iterrows():
                            for class_period in PERIODS:
                                if self.get_mark_format(class_period, self.semester, self.quarter) in row:
                                    if row[self.get_mark_format(class_period, self.semester, self.quarter)] == 'F' or row[self.get_mark_format(class_period, self.semester, self.quarter)] == 'NP':
                                        student_story = []
                                        student_story.append(row['Student Name'])
                                        student_story.append(str(row['Student ID']))
                                        student_story.append(row[self.get_course_format(class_period, self.semester)])
                                        student_story.append(row[self.get_teacher_format(class_period, self.semester)])
                                        student_story.append(str(class_period))
                                        student_story.append(row[self.get_mark_format(class_period, self.semester, self.quarter)])
                                        student_story.append("Off-Track (Red)")
                                        table_story.append(student_story)
            #print("Debug Table Story:", table_story)
            if table_story:
                table = Table(table_story)
                table.setStyle([('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                        ('BOX', (0,0), (-1,-1), 0.25, colors.black)])
                story.append(table)
            c.build(story)

            if password:
                document = PyPDF2.PdfFileReader('output/access_yellow_report/' + teacher_name + '.pdf')
                document_writer = PyPDF2.PdfFileWriter()
                for page in range(document.getNumPages()):
                    document_writer.addPage(document.getPage(page))
                document_writer.encrypt(user_pwd=password, owner_pwd=None, use_128bit=True)
                with open('output/access_yellow_report/' + teacher_name + '.pdf', 'wb') as file_handler:
                    document_writer.write(file_handler)
            message.tick_access_bar.emit()"""
        access_workbooks = None
        message.set_status.emit("Access Reports: Completed.") 
    
    def access_reports_old(self, password):
        # Pre-2022 method
        if not os.path.exists('output/access_yellow_report'):
            os.makedirs('output/access_yellow_report')
        access_workbook, sheet = self.fresh_sheet()
        message.set_status.emit("Generating access reports.")

        raw_rows = sheet.max_row - 2
        access_rows = 0
        for x in self.full_data['RR Teacher']:
            if self.is_access(x):
                access_rows += 1
        
        message.set_access_bar_max.emit(int(len(tuple(sheet.columns)) + len(self.access_teacher_names)*2 + raw_rows-2))

        access_teacher_column = 1
        message.set_status.emit("Access reports: Processing Excel Matrix")

        for row in sheet.iter_rows(min_row=2, max_row=2):
            for cell in row:
                if cell.value == 'RR Teacher' or cell.value == 'Access / AVID Teacher':
                    access_teacher_column = cell.column
                message.tick_access_bar.emit()

        print("Debug: Access column:", access_teacher_column)
        message.set_status.emit("Access reports: Initial Optimization")

        # {partner_name: [workbook, worksheet, row_counter]}
        access_workbooks = {}

        for name in self.access_teacher_names:
            message.set_status.emit("Creating access reports: " + name)
            access_workbook, access_sheet = self.fresh_template_sheet()
            message.set_status.emit("Access reports: Trimming book for: " + name)
            access_sheet.title = name
            access_workbooks[name] = [access_workbook, access_sheet, 3]
            message.tick_access_bar.emit()

        message.set_status.emit("Access reports phase 1/2: Generating Individual Access/AVID Reports")
        n = 0
        for row in sheet.iter_rows(min_row=3):
            message.set_status.emit("Creating access reports: " + str(n) + '/' + str(sheet.max_row))
            access_name = row[access_teacher_column-1].value # -1 due to index of 0 in python vs index of 1 in openpyexcel
            if not self.is_access(access_name):
                message.tick_access_bar.emit()
                n += 1
                #print("No dice:", access_name, self.access_teacher_names)
                continue
            # Need to migrade to a better system for this middle initial problem
            if access_name[-1] == '.':
                access_name = access_name[:-3]
            p_sheet = access_workbooks[access_name][1]
            p_row = access_workbooks[access_name][2]
            for cell in row:
                new_cell = p_sheet.cell(row=p_row, column=cell.col_idx, value=cell.value)
                new_cell = self.copy_cell(cell, new_cell)
            access_workbooks[access_name][2] += 1
            message.tick_access_bar.emit()
            n += 1

        styles=getSampleStyleSheet()
        for teacher_name in self.access_teacher_names:
            message.set_status.emit("Access reports phase 2/2: Generating report: " + teacher_name)
            # TODO: Iterate the rows of the prunned access_workbook just above
            # using for index, row in full_data.iterrows(): near the top. This way I
            # wont have to deal with the middle initial sadness in RR Teacher.
            access_workbooks[teacher_name][0].save('output/access_yellow_report/' + str(teacher_name) + '.xlsx')
            access_workbooks[teacher_name][0].close()

            formatted_time = time.ctime()
            story = []
            c = SimpleDocTemplate('output/access_yellow_report/' + teacher_name + '.pdf', pagesize=letter)
            width, height = letter
            ptext = "CONFIDENTIAL: FERPA!"
            story.append(Paragraph(ptext, styles["Heading1"]))
            ptext = "Failing student report for " + str(teacher_name) + ". Generated on: " + str(formatted_time)
            story.append(Paragraph(ptext, styles["Normal"]))
            ptext = "I'm a bot. Beep Boop. The following students in your access classes are failing 2 or more classes. Yellow zone students are failing 2-3 classes. Red zone students are failing 4-7 classes."
            story.append(Paragraph(ptext, styles["Normal"]))
            table_story = []

            teacher_set = pd.read_excel('output/access_yellow_report/' + str(teacher_name) + '.xlsx', header=1)
            for period in PERIODS:
                class_set = teacher_set[teacher_set['RR Period'] == period]
                #print("Debug Class set:", teacher_name, period)
                if not class_set.empty:
                    class_set_yellow = class_set[class_set[self.get_gradezone_format(self.quarter, self.semester)] == 'At-Risk']
                    class_set_red = class_set[class_set[self.get_gradezone_format(self.quarter, self.semester)] == 'Off-Track']
                    #print("Debug yellow:", class_set_yellow)
                    #print("Debug red:", class_set_red)
                    if not class_set_yellow.empty or not class_set_red.empty:
                        ptext = 'Period ' + str(period) + ':'
                        table_story.append([ptext, '', '','','','',''])
                        table_story.append(['Student', 'ID', 'Class', 'Teacher', 'Period', 'Grade', 'Zone'])
                        for index, row in class_set_yellow.iterrows():
                            for class_period in PERIODS:
                                if self.get_mark_format(class_period, self.semester, self.quarter) in row:
                                    if row[self.get_mark_format(class_period, self.semester, self.quarter)] == 'F' or row[self.get_mark_format(class_period, self.semester, self.quarter)] == 'NP':
                                        student_story = []
                                        student_story.append(row['Student Name'])
                                        student_story.append(str(row['Student ID']))
                                        student_story.append(row[self.get_course_format(class_period, self.semester)])
                                        student_story.append(row[self.get_teacher_format(class_period, self.semester)])
                                        student_story.append(str(class_period))
                                        student_story.append(row[self.get_mark_format(class_period, self.semester, self.quarter)])
                                        student_story.append("At-Risk (Yellow)")
                                        table_story.append(student_story)
                        for index, row in class_set_red.iterrows():
                            for class_period in PERIODS:
                                if self.get_mark_format(class_period, self.semester, self.quarter) in row:
                                    if row[self.get_mark_format(class_period, self.semester, self.quarter)] == 'F' or row[self.get_mark_format(class_period, self.semester, self.quarter)] == 'NP':
                                        student_story = []
                                        student_story.append(row['Student Name'])
                                        student_story.append(str(row['Student ID']))
                                        student_story.append(row[self.get_course_format(class_period, self.semester)])
                                        student_story.append(row[self.get_teacher_format(class_period, self.semester)])
                                        student_story.append(str(class_period))
                                        student_story.append(row[self.get_mark_format(class_period, self.semester, self.quarter)])
                                        student_story.append("Off-Track (Red)")
                                        table_story.append(student_story)
            #print("Debug Table Story:", table_story)
            if table_story:
                table = Table(table_story)
                table.setStyle([('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                        ('BOX', (0,0), (-1,-1), 0.25, colors.black)])
                story.append(table)
            c.build(story)

            if password:
                document = PyPDF2.PdfFileReader('output/access_yellow_report/' + teacher_name + '.pdf')
                document_writer = PyPDF2.PdfFileWriter()
                for page in range(document.getNumPages()):
                    document_writer.addPage(document.getPage(page))
                document_writer.encrypt(user_pwd=password, owner_pwd=None, use_128bit=True)
                with open('output/access_yellow_report/' + teacher_name + '.pdf', 'wb') as file_handler:
                    document_writer.write(file_handler)
            message.tick_access_bar.emit()
        access_workbooks = None
        message.set_status.emit("Access Reports: Completed.")

    def teacher_reports(self, pdf=False, excel=False, password=None, email=False):
        if not os.path.exists('output/teacher_yellow_report'):
            os.makedirs('output/teacher_yellow_report')
        teacher_workbook, sheet = self.fresh_sheet()
        if pdf and not excel:
            message.set_teacher_bar_max.emit(int(len(self.teacher_names) * len(PERIODS) - 1))
        else:
            message.set_teacher_bar_max.emit(int(len(self.teacher_names) * len(PERIODS) - 1 + len(self.teacher_names)*2 + (sheet.max_row - 3)))
        styles = getSampleStyleSheet()
        teacher_workbooks = {}

        if excel:
            ### Temp all teacher report file
            # {teacher_name: [workbook, worksheet, row_counter]}
            for teacher_name in self.teacher_names:
                message.set_status.emit("Creating individual teacher reports: " + teacher_name)
                teacher_workbook, teacher_sheet = self.fresh_template_sheet()
                message.set_status.emit("Teacher reports: Trimming book for: " + teacher_name)
                teacher_sheet.title = teacher_name
                teacher_workbooks[teacher_name] = [teacher_workbook, teacher_sheet, 3]
                message.tick_teacher_bar.emit()
            
            period_name_columns = {} # Columns of the name of teachers by periods
            for p in PERIODS:
                period_name_columns[p] = None
            for row in sheet.iter_rows(min_row=2, max_row=2):
                for period in PERIODS:
                    for cell in row:
                        if self.is_name(cell.value, self.get_teacher_format(period, self.semester)):
                            period_name_columns[period] = cell.column
                            break
            
            ### Temp all teacher report file
            # {teacher_name: [workbook, worksheet, row_counter]}
            n = 0
            for row in sheet.iter_rows(min_row=3):
                message.set_status.emit("Creating individual teacher reports: " + str(n) + '/' + str(sheet.max_row))
                for period in PERIODS: 
                    if period_name_columns[period] is not None:
                        teacher_name = row[period_name_columns[period] - 1].value # -1 due to index of 0 in python vs index of 1 in openpyexcel
                        if self.is_teacher(teacher_name):
                            t_sheet = teacher_workbooks[teacher_name][1]
                            t_row = teacher_workbooks[teacher_name][2]
                            for cell in row:
                                new_cell = t_sheet.cell(row=t_row, column=cell.col_idx, value=cell.value)
                                new_cell = self.copy_cell(cell, new_cell)
                            teacher_workbooks[teacher_name][2] += 1
                message.tick_teacher_bar.emit()
                n += 1

            #pythoncom.CoInitialize()
            for teacher_name in self.teacher_names:
                message.set_status.emit("Saving individual teacher reports: " + teacher_name)
                filename = f'output/teacher_yellow_report/{teacher_name}.xlsx'
                filename_password = f'output/teacher_yellow_report/{teacher_name}.password.xlsx'
                teacher_workbooks[teacher_name][0].save(filename)
                teacher_workbooks[teacher_name][0].close()
                #if password:
                #    cwd = os.getcwd()
                #    excel_file_path = os.path.abspath(os.path.join(cwd, filename))
                #    excel_file_path_password = os.path.abspath(os.path.join(cwd, filename_password))
                #    xcl = win32com.client.Dispatch('Excel.Application')
                #    wb = xcl.Workbooks.Open(excel_file_path, False, False, None)
                #    wb.SaveAs(excel_file_path_password, None, password)
                #    xcl.Quit()
                message.tick_teacher_bar.emit()
        teacher_workbooks = {}

        # F and Ds
        if pdf:
            for teacher_name in self.teacher_names:
                message.set_status.emit("Creating individual teacher pdf: " + teacher_name)
                formatted_time = time.ctime()
                story = []
                c = SimpleDocTemplate('output/teacher_yellow_report/' + teacher_name + '.pdf', pagesize=letter)
                ptext = "CONFIDENTIAL: FERPA!"
                story.append(Paragraph(ptext, styles["Heading3"]))
                ptext = "Failing student report for " + str(teacher_name) + ". Generated on: " + str(formatted_time)
                story.append(Paragraph(ptext, styles["Normal"]))
                ptext = """I'm a bot. Beep Boop. The following students are in danger of failing your class.
                Yellow zone students are passing 6-7 classes. Red zone students are passing 5 or less."""
                story.append(Paragraph(ptext, styles["Normal"]))
                table_story = []
                # At this point this is almost worth making a class
                for period in PERIODS:
                    if self.get_teacher_format(period, self.semester) in self.full_data:
                        class_set = self.full_data[self.full_data[self.get_teacher_format(period, self.semester)] == teacher_name]
                        #class_set.to_pickle('output/teacher_pickle/' + teacher_name + '_' + str(period)) 
                        class_set_red = class_set[class_set[self.get_gradezone_format(self.quarter, self.semester)] == 'Off-Track']
                        class_set_yellow = class_set[class_set[self.get_gradezone_format(self.quarter, self.semester)] == 'At-Risk']
                        class_set_green = class_set[class_set[self.get_gradezone_format(self.quarter, self.semester)] == 'On-Track']
                        green_inclass = class_set_green[class_set_green[self.get_mark_format(period, self.semester, self.quarter)] == 'D']
                        green_inclass = pd.concat([green_inclass, class_set_green[class_set_green[self.get_mark_format(period, self.semester, self.quarter)] == 'NP']])
                        green_inclass = pd.concat([green_inclass, class_set_green[class_set_green[self.get_mark_format(period, self.semester, self.quarter)] == 'F']])
                        green_students = list(green_inclass['Student Name'].values)
                        yellow_inclass = class_set_yellow[class_set_yellow[self.get_mark_format(period, self.semester, self.quarter)] == 'D']
                        yellow_inclass = pd.concat([yellow_inclass, class_set_yellow[class_set_yellow[self.get_mark_format(period, self.semester, self.quarter)] == 'NP']])
                        yellow_inclass = pd.concat([yellow_inclass, class_set_yellow[class_set_yellow[self.get_mark_format(period, self.semester, self.quarter)] == 'F']])
                        yellow_students = list(yellow_inclass['Student Name'].values)
                        red_inclass = class_set_red[class_set_red[self.get_mark_format(period, self.semester, self.quarter)] == 'D']
                        red_inclass = pd.concat([red_inclass, class_set_red[class_set_red[self.get_mark_format(period, self.semester, self.quarter)] == 'NP']])
                        red_inclass = pd.concat([red_inclass, class_set_red[class_set_red[self.get_mark_format(period, self.semester, self.quarter)] == 'F']])
                        red_students = list(red_inclass['Student Name'].values)
                        if yellow_students or red_students or green_students:
                            ptext = 'Period ' + str(period) + ':'
                            table_story.append([ptext, 'Grade', 'Zone', 'Attendance'])
                            for student in green_students:
                                ptext = student
                                grade_text = class_set_green.loc[class_set_green['Student Name'] == student][self.get_mark_format(period, self.semester, self.quarter)].values[0]
                                att_text = ""
                                try:
                                    att_text = class_set_green.loc[class_set_green['Student Name'] == student]["YTD ATT Zone"].values[0]
                                except:
                                    print("Error with:", student)
                                table_story.append([ptext, grade_text, "Green", att_text])
                            for student in yellow_students:
                                ptext = student
                                grade_text = class_set_yellow.loc[class_set_yellow['Student Name'] == student][self.get_mark_format(period, self.semester, self.quarter)].values[0]
                                att_text = ""
                                try:
                                    att_text = class_set_yellow.loc[class_set_yellow['Student Name'] == student]["YTD ATT Zone"].values[0]
                                except:
                                    print("Error with:", student)
                                table_story.append([ptext, grade_text, "Yellow", att_text])
                            for student in red_students:
                                ptext = student
                                grade_text = class_set_red.loc[class_set_red['Student Name'] == student][self.get_mark_format(period, self.semester, self.quarter)].values[0]
                                att_text = ""
                                try:
                                    att_text = class_set_red.loc[class_set_red['Student Name'] == student]["YTD ATT Zone"].values[0]
                                except:
                                    print("Error with:", student)
                                table_story.append([ptext, grade_text, "Red", att_text])
                    table_story.append(['', '', '', ''])        
                    message.tick_teacher_bar.emit()
                if table_story:
                    table = Table(table_story)
                    table.setStyle([('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                            ('BOX', (0,0), (-1,-1), 0.25, colors.black)])
                    story.append(table)
                c.build(story)
                
                document = PyPDF2.PdfReader('output/teacher_yellow_report/' + teacher_name + '.pdf')
                document_writer = PyPDF2.PdfWriter()
                for page in range(len(document.pages)):
                    document_writer.add_page(document.pages[page])
                if password:
                    document_writer.encrypt(user_pwd=password, owner_pwd=None, use_128bit=True)
                with open('output/teacher_yellow_report/' + teacher_name + '.pdf', 'wb') as file_handler:
                    document_writer.write(file_handler)

        # Orig! It works. F only
        """if pdf:
            for teacher_name in self.teacher_names:
                message.set_status.emit("Creating individual teacher pdf: " + teacher_name)
                formatted_time = time.ctime()
                story = []
                c = SimpleDocTemplate('output/teacher_yellow_report/' + teacher_name + '.pdf', pagesize=letter)
                ptext = "CONFIDENTIAL: FERPA!"
                story.append(Paragraph(ptext, styles["Heading1"]))
                ptext = "Failing student report for " + str(teacher_name) + ". Generated on: " + str(formatted_time)
                story.append(Paragraph(ptext, styles["Normal"]))
                ptext = "I'm a bot. Beep Boop. The following students are currently not passing your class. Yellow zone students are failing 2-3 classes. Red zone students are failing 4-7 classes."
                story.append(Paragraph(ptext, styles["Normal"]))
                table_story = []
                for period in PERIODS:
                    if self.get_teacher_format(period, self.semester) in self.full_data:
                        class_set = self.full_data[self.full_data[self.get_teacher_format(period, self.semester)] == teacher_name]
                        class_set_red = class_set[class_set[self.get_gradezone_format(self.quarter, self.semester)] == 'Off-Track']
                        class_set_yellow = class_set[class_set[self.get_gradezone_format(self.quarter, self.semester)] == 'At-Risk']
                        class_set_green = class_set[class_set[self.get_gradezone_format(self.quarter, self.semester)] == 'On-Track']
                        green_inclass = class_set_green[class_set_green[self.get_mark_format(period, self.semester, self.quarter)] == 'F']
                        green_inclass = pd.concat([green_inclass, class_set_green[class_set_green[self.get_mark_format(period, self.semester, self.quarter)] == 'NP']])
                        green_students = list(green_inclass['Student Name'].values)
                        yellow_inclass = class_set_yellow[class_set_yellow[self.get_mark_format(period, self.semester, self.quarter)] == 'F']
                        yellow_inclass = pd.concat([yellow_inclass, class_set_yellow[class_set_yellow[self.get_mark_format(period, self.semester, self.quarter)] == 'NP']])
                        yellow_students = list(yellow_inclass['Student Name'].values)
                        red_inclass = class_set_red[class_set_red[self.get_mark_format(period, self.semester, self.quarter)] == 'F']
                        red_inclass = pd.concat([red_inclass, class_set_red[class_set_red[self.get_mark_format(period, self.semester, self.quarter)] == 'NP']])
                        red_students = list(red_inclass['Student Name'].values)
                        if yellow_students or red_students or green_students:
                            ptext = 'Period ' + str(period) + ':'
                            table_story.append([ptext, ''])
                            for student in green_students:
                                ptext = student
                                table_story.append([ptext, "Green"])
                            for student in yellow_students:
                                ptext = student
                                table_story.append([ptext, "Yellow"])
                            for student in red_students:
                                ptext = student
                                table_story.append([ptext, "Red"])
                    message.tick_teacher_bar.emit()
                if table_story:
                    table = Table(table_story)
                    table.setStyle([('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                            ('BOX', (0,0), (-1,-1), 0.25, colors.black)])
                    story.append(table)
                c.build(story)
                
                document = PyPDF2.PdfFileReader('output/teacher_yellow_report/' + teacher_name + '.pdf')
                document_writer = PyPDF2.PdfFileWriter()
                for page in range(document.getNumPages()):
                    document_writer.addPage(document.getPage(page))
                if password:
                    document_writer.encrypt(user_pwd=password, owner_pwd=None, use_128bit=True)
                with open('output/teacher_yellow_report/' + teacher_name + '.pdf', 'wb') as file_handler:
                    document_writer.write(file_handler)"""
        
        message.set_status.emit('Teacher report: DONE!')
      

class ReportEngineV1N2023(ReportEngineV1N2020):
    '''Revised for the 2023 update, some changes in format and no community partners'''
    def get_course_format(self, period, semester) -> str:
        return f'S{semester} P{period} COURSE'
    
    def get_teacher_format(self, period, semester) -> str:
        return f'S{semester} P{period} TEACHER'

    def get_gradezone_format(self, quarter, semester) -> str:
        return f'S{semester} Q{quarter}\nStudent\nMarks\nZone'

    def get_mark_format(self, period, semester, quarter) -> str:
        # ????
        return f"S{semester} P{period}\nMARK Q{quarter}"
    
    def load(self, path):
        '''Load an excel file on path.'''
        self.path = path
        self.excel_pandas = pd.ExcelFile(path)
        message.finished_inital_extraction.emit()
        print("Debug: Loaded using V1N2023")

    def teacher_reports(self, pdf=False, excel=False, password=None, email=False):
        if not os.path.exists('output/teacher_yellow_report'):
            os.makedirs('output/teacher_yellow_report')
        if not os.path.exists('output/teacher_eld_report'):
            os.makedirs('output/teacher_eld_report')
        teacher_workbook, sheet = self.fresh_sheet()
        if pdf and not excel:
            message.set_teacher_bar_max.emit(int(len(self.teacher_names) * len(PERIODS) - 1) * 2)
        else:
            message.set_teacher_bar_max.emit(int(len(self.teacher_names) * len(PERIODS) - 1 + len(self.teacher_names)*2 + (sheet.max_row - 3)))
        styles = getSampleStyleSheet()
        teacher_workbooks = {}

        if excel:
            ### Temp all teacher report file
            # {teacher_name: [workbook, worksheet, row_counter]}
            for teacher_name in self.teacher_names:
                message.set_status.emit("Creating individual teacher reports: " + teacher_name)
                teacher_workbook, teacher_sheet = self.fresh_template_sheet()
                message.set_status.emit("Teacher reports: Trimming book for: " + teacher_name)
                teacher_sheet.title = teacher_name
                teacher_workbooks[teacher_name] = [teacher_workbook, teacher_sheet, 3]
                message.tick_teacher_bar.emit()
            
            period_name_columns = {} # Columns of the name of teachers by periods
            for p in PERIODS:
                period_name_columns[p] = None
            for row in sheet.iter_rows(min_row=2, max_row=2):
                for period in PERIODS:
                    for cell in row:
                        if self.is_name(cell.value, self.get_teacher_format(period, self.semester)):
                            period_name_columns[period] = cell.column
                            break
            
            ### Temp all teacher report file
            # {teacher_name: [workbook, worksheet, row_counter]}
            n = 0
            for row in sheet.iter_rows(min_row=3):
                message.set_status.emit("Creating individual teacher reports: " + str(n) + '/' + str(sheet.max_row))
                for period in PERIODS: 
                    if period_name_columns[period] is not None:
                        teacher_name = row[period_name_columns[period] - 1].value # -1 due to index of 0 in python vs index of 1 in openpyexcel
                        if self.is_teacher(teacher_name):
                            t_sheet = teacher_workbooks[teacher_name][1]
                            t_row = teacher_workbooks[teacher_name][2]
                            for cell in row:
                                new_cell = t_sheet.cell(row=t_row, column=cell.col_idx, value=cell.value)
                                new_cell = self.copy_cell(cell, new_cell)
                            teacher_workbooks[teacher_name][2] += 1
                message.tick_teacher_bar.emit()
                n += 1

            #pythoncom.CoInitialize()
            for teacher_name in self.teacher_names:
                message.set_status.emit("Saving individual teacher reports: " + teacher_name)
                filename = f'output/teacher_yellow_report/{teacher_name}.xlsx'
                filename_password = f'output/teacher_yellow_report/{teacher_name}.password.xlsx'
                teacher_workbooks[teacher_name][0].save(filename)
                teacher_workbooks[teacher_name][0].close()
                #if password:
                #    cwd = os.getcwd()
                #    excel_file_path = os.path.abspath(os.path.join(cwd, filename))
                #    excel_file_path_password = os.path.abspath(os.path.join(cwd, filename_password))
                #    xcl = win32com.client.Dispatch('Excel.Application')
                #    wb = xcl.Workbooks.Open(excel_file_path, False, False, None)
                #    wb.SaveAs(excel_file_path_password, None, password)
                #    xcl.Quit()
                message.tick_teacher_bar.emit()
        teacher_workbooks = {}

        # F and Ds
        if pdf:
            for teacher_name in self.teacher_names:
                message.set_status.emit("Creating individual teacher zone pdf: " + teacher_name)
                formatted_time = time.ctime()
                story = []
                c = SimpleDocTemplate('output/teacher_yellow_report/' + teacher_name + '.pdf', pagesize=letter)
                ptext = "CONFIDENTIAL: FERPA!"
                story.append(Paragraph(ptext, styles["Heading3"]))
                ptext = "Failing student report for " + str(teacher_name) + ". Generated on: " + str(formatted_time)
                story.append(Paragraph(ptext, styles["Normal"]))
                ptext = """I'm a bot. Beep Boop. The following students are in danger of failing your class.
                Yellow zone students are passing 6-7 classes. Red zone students are passing 5 or less."""
                story.append(Paragraph(ptext, styles["Normal"]))
                table_story = []
                # At this point this is almost worth making a class
                for period in PERIODS:
                    if self.get_teacher_format(period, self.semester) in self.full_data:
                        class_set = self.full_data[self.full_data[self.get_teacher_format(period, self.semester)] == teacher_name]
                        #class_set.to_pickle('output/teacher_pickle/' + teacher_name + '_' + str(period)) 
                        class_set_red = class_set[class_set[self.get_gradezone_format(self.quarter, self.semester)] == 'Off-Track']
                        class_set_yellow = class_set[class_set[self.get_gradezone_format(self.quarter, self.semester)] == 'At-Risk']
                        class_set_green = class_set[class_set[self.get_gradezone_format(self.quarter, self.semester)] == 'On-Track']
                        green_inclass = class_set_green[class_set_green[self.get_mark_format(period, self.semester, self.quarter)] == 'D']
                        green_inclass = pd.concat([green_inclass, class_set_green[class_set_green[self.get_mark_format(period, self.semester, self.quarter)] == 'NP']])
                        green_inclass = pd.concat([green_inclass, class_set_green[class_set_green[self.get_mark_format(period, self.semester, self.quarter)] == 'F']])
                        green_students = list(green_inclass['Student Name'].values)
                        yellow_inclass = class_set_yellow[class_set_yellow[self.get_mark_format(period, self.semester, self.quarter)] == 'D']
                        yellow_inclass = pd.concat([yellow_inclass, class_set_yellow[class_set_yellow[self.get_mark_format(period, self.semester, self.quarter)] == 'NP']])
                        yellow_inclass = pd.concat([yellow_inclass, class_set_yellow[class_set_yellow[self.get_mark_format(period, self.semester, self.quarter)] == 'F']])
                        yellow_students = list(yellow_inclass['Student Name'].values)
                        red_inclass = class_set_red[class_set_red[self.get_mark_format(period, self.semester, self.quarter)] == 'D']
                        red_inclass = pd.concat([red_inclass, class_set_red[class_set_red[self.get_mark_format(period, self.semester, self.quarter)] == 'NP']])
                        red_inclass = pd.concat([red_inclass, class_set_red[class_set_red[self.get_mark_format(period, self.semester, self.quarter)] == 'F']])
                        red_students = list(red_inclass['Student Name'].values)
                        if yellow_students or red_students or green_students:
                            ptext = 'Period ' + str(period) + ':'
                            table_story.append([ptext, 'Grade', 'Zone', 'Attendance'])
                            for student in green_students:
                                ptext = student
                                grade_text = class_set_green.loc[class_set_green['Student Name'] == student][self.get_mark_format(period, self.semester, self.quarter)].values[0]
                                att_text = ""
                                try:
                                    att_text = class_set_green.loc[class_set_green['Student Name'] == student]["YTD ATT Zone"].values[0]
                                except:
                                    print("Error with:", student)
                                table_story.append([ptext, grade_text, "Green", att_text])
                            for student in yellow_students:
                                ptext = student
                                grade_text = class_set_yellow.loc[class_set_yellow['Student Name'] == student][self.get_mark_format(period, self.semester, self.quarter)].values[0]
                                att_text = ""
                                try:
                                    att_text = class_set_yellow.loc[class_set_yellow['Student Name'] == student]["YTD ATT Zone"].values[0]
                                except:
                                    print("Error with:", student)
                                table_story.append([ptext, grade_text, "Yellow", att_text])
                            for student in red_students:
                                ptext = student
                                grade_text = class_set_red.loc[class_set_red['Student Name'] == student][self.get_mark_format(period, self.semester, self.quarter)].values[0]
                                att_text = ""
                                try:
                                    att_text = class_set_red.loc[class_set_red['Student Name'] == student]["YTD ATT Zone"].values[0]
                                except:
                                    print("Error with:", student)
                                table_story.append([ptext, grade_text, "Red", att_text])
                    table_story.append(['', '', '', ''])        
                    message.tick_teacher_bar.emit()
                if table_story:
                    table = Table(table_story)
                    table.setStyle([('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                            ('BOX', (0,0), (-1,-1), 0.25, colors.black)])
                    story.append(table)
                c.build(story)
                
                document = PyPDF2.PdfReader('output/teacher_yellow_report/' + teacher_name + '.pdf')
                document_writer = PyPDF2.PdfWriter()
                for page in range(len(document.pages)):
                    document_writer.add_page(document.pages[page])
                if password:
                    document_writer.encrypt(user_pwd=password, owner_pwd=None, use_128bit=True)
                with open('output/teacher_yellow_report/' + teacher_name + '.pdf', 'wb') as file_handler:
                    document_writer.write(file_handler)
            
            for teacher_name in self.teacher_names:
                message.set_status.emit("Creating individual teacher eld pdf: " + teacher_name)
                formatted_time = time.ctime()
                story = []
                c = SimpleDocTemplate('output/teacher_eld_report/' + teacher_name + '.pdf', pagesize=letter)
                ptext = "CONFIDENTIAL: FERPA!"
                story.append(Paragraph(ptext, styles["Heading3"]))
                ptext = "ELD student report for " + str(teacher_name) + ". Generated on: " + str(formatted_time)
                story.append(Paragraph(ptext, styles["Normal"]))
                ptext = """I'm a bot. Beep Boop. The following students are enrolled in an ELD class."""
                story.append(Paragraph(ptext, styles["Normal"]))
                table_story = []
                # At this point this is almost worth making a class
                for period in PERIODS:
                    if self.get_teacher_format(period, self.semester) in self.full_data:
                        class_set = self.full_data[self.full_data[self.get_teacher_format(period, self.semester)] == teacher_name]
                        ptext = 'Period ' + str(period) + ':'
                        table_story.append([ptext, 'ELD Course', 'Teacher', 'Period'])
                        for index, row in class_set.iterrows():
                            for eld_period in PERIODS:
                                if self.get_course_format(eld_period, self.semester) in row:
                                    if type(row[self.get_course_format(eld_period, self.semester)]) is str and 'eld' in row[self.get_course_format(eld_period, self.semester)].lower():
                                        eld_class_name = row[f'S1 P{eld_period} COURSE'].split('-', 1)[1]
                                        eld_teacher = row[self.get_teacher_format(eld_period, self.semester)]
                                        ptext = row['Student Name']
                                        table_story.append([ptext, eld_class_name, eld_teacher, eld_period])
                        table_story.append(['', '', '', ''])  
                    message.tick_teacher_bar.emit()
                if table_story:
                    table = Table(table_story)
                    table.setStyle([('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                            ('BOX', (0,0), (-1,-1), 0.25, colors.black)])
                    story.append(table)
                c.build(story)
                
                document = PyPDF2.PdfReader('output/teacher_eld_report/' + teacher_name + '.pdf')
                document_writer = PyPDF2.PdfWriter()
                for page in range(len(document.pages)):
                    document_writer.add_page(document.pages[page])
                if password:
                    document_writer.encrypt(user_pwd=password, owner_pwd=None, use_128bit=True)
                with open('output/teacher_eld_report/' + teacher_name + '.pdf', 'wb') as file_handler:
                    document_writer.write(file_handler)
        message.set_status.emit('Teacher report: DONE!')
    
    def community_reports(self):
        # Create a temp file to store an excel sheet that has only has community partner
        # students. This will greatly speed up run time
        message.set_status.emit("Generating community partner reports.")
        if not os.path.exists('output/community_partners'):
            os.makedirs('output/community_partners')
        partner_name_column = 0
        partner_workbook, sheet = self.fresh_sheet()
        message.set_status.emit("Partner reports: Processing Excel Matrix...")

        for row in sheet.iter_rows(min_row=2, max_row=2):
            for cell in row:
                if cell.value == 'Community Partner\nOrganization':
                    partner_name_column = cell.column
        
        print("DEBUG: Community Partner Report: Name Column:", partner_name_column)
        
        message.set_partner_bar_max.emit(int(sheet.max_row - 2 + len(self.community_partners)))
        # {partner_name: [workbook, worksheet, row_counter]}
        partner_workbooks = {}

        for partner_name in self.community_partners:
            message.set_status.emit("Creating partner reports: " + partner_name)
            partner_workbook, partner_sheet = self.fresh_template_sheet()
            message.set_status.emit("Partner reports: Trimming book for: " + partner_name)
            partner_sheet.title = partner_name
            partner_workbooks[partner_name] = [partner_workbook, partner_sheet, 3]
            message.tick_partner_bar.emit()
        
        n = 0
        for row in sheet.iter_rows(min_row=3):
            message.set_status.emit("Creating partner reports: " + str(n) + '/' + str(sheet.max_row))
            partner_name = row[partner_name_column-1].value # -1 due to index of 0 in python vs index of 1 in openpyexcel
            if partner_name not in self.community_partners:
                message.tick_partner_bar.emit()
                n += 1
                continue
            p_sheet = partner_workbooks[partner_name][1]
            p_row = partner_workbooks[partner_name][2]
            for cell in row:
                new_cell = p_sheet.cell(row=p_row, column=cell.col_idx, value=cell.value)
                new_cell = self.copy_cell(cell, new_cell)
            partner_workbooks[partner_name][2] += 1
            message.tick_partner_bar.emit()
            n += 1

        for partner_name in self.community_partners:
            message.set_status.emit("Saving individual partner reports: " + partner_name)
            partner_workbooks[partner_name][0].save('output/community_partners/' + str(partner_name) + '.xlsx')
            partner_workbooks[partner_name][0].close()
            message.tick_partner_bar.emit()
        
        partner_workbooks = None
        message.set_status.emit('Partner report: DONE!')
    
    def access_reports(self, password):
        if not os.path.exists('output/access_yellow_report'):
            os.makedirs('output/access_yellow_report')
        
        message.set_status.emit("Setting up access reports.")
        access_workbook, sheet = self.fresh_sheet()
        raw_rows = sheet.max_row - 2
        
        message.set_access_bar_max.emit(int(len(tuple(sheet.columns)) + len(self.access_teacher_names)*2 + raw_rows-2))

        # {access_name: [workbook, worksheet, row_counter]}
        access_workbooks = {}
        # {period: (courlse column, teacher column)}
        class_columns = {}
        for period in PERIODS:
            class_columns[period] = (1, 1)

        for row in sheet.iter_rows(min_row=2, max_row=2):
            for cell in row:
                for period in PERIODS:
                    if cell.value == self.get_course_format(period, self.semester):
                        class_columns[period] = (cell.column-1, cell.column)
        
        print('DEBUG: Access Reports: class_columns:', class_columns)

        access_classes = ['9th Grade Access', 'AVID 9', 'AVID 10', 'AVID 11', 'AVID 12', 'Strategies for Success 9-10', 'Academic Skills Tutorial', 'ELD Access']

        message.set_status.emit("Generating access reports.")

        for access_name in self.access_teacher_names:
            message.set_status.emit("Creating individual access reports: " + access_name)
            access_workbook, access_sheet = self.fresh_template_sheet()
            message.set_status.emit("Access reports: Trimming book for: " + access_name)
            access_sheet.title = access_name
            access_workbooks[access_name] = [access_workbook, access_sheet, 3]
            message.tick_access_bar.emit()
        
        message.set_status.emit("Access reports phase 1/2: Generating Individual Access/AVID Reports")
        n = 0
        for row in sheet.iter_rows(min_row=3):
            message.set_status.emit("Creating individual access reports: " + str(n) + '/' + str(sheet.max_row))
            for period in PERIODS:
                class_column, teacher_column = class_columns[period]
                class_name = row[class_column].value
                if class_name and class_name.split('-', 1)[-1] in access_classes:
                    access_name = row[teacher_column].value
                    c_sheet = access_workbooks[access_name][1]
                    c_row = access_workbooks[access_name][2]
                    for cell in row:
                        new_cell = c_sheet.cell(row=c_row, column=cell.col_idx, value=cell.value)
                        new_cell = self.copy_cell(cell, new_cell)
                    access_workbooks[access_name][2] += 1
                    break

            message.tick_access_bar.emit()
            n += 1
        
        styles=getSampleStyleSheet()
        for teacher_name in self.access_teacher_names:
            message.set_status.emit("Access reports phase 2/2: Generating report: " + teacher_name)
            # TODO: Iterate the rows of the prunned access_workbook just above
            # using for index, row in full_data.iterrows(): near the top. This way I
            # wont have to deal with the middle initial sadness in RR Teacher.
            access_workbooks[teacher_name][0].save('output/access_yellow_report/' + str(teacher_name) + '.xlsx')
            access_workbooks[teacher_name][0].close()

            formatted_time = time.ctime()
            story = []
            c = SimpleDocTemplate('output/access_yellow_report/' + teacher_name + '.pdf', pagesize=letter)
            width, height = letter
            ptext = "CONFIDENTIAL: FERPA!"
            story.append(Paragraph(ptext, styles["Heading1"]))
            ptext = "Failing student report for " + str(teacher_name) + ". Generated on: " + str(formatted_time)
            story.append(Paragraph(ptext, styles["Normal"]))
            ptext = "I'm a bot. Beep Boop. The following students in your access/AVID/Skills classes are failing 2 or more classes. Yellow zone students are failing 2-3 classes. Red zone students are failing 4-7 classes."
            story.append(Paragraph(ptext, styles["Normal"]))
            table_story = []

            teacher_set = pd.read_excel('output/access_yellow_report/' + str(teacher_name) + '.xlsx', header=1)
            for period in PERIODS:
                for access_class_name in access_classes:
                    #TODO: Complete the pdf generation
                    if f'{period}-{access_class_name}' not in teacher_set:
                        continue
                    class_set = teacher_set[teacher_set[self.get_course_format(period, self.semester)] == f'{period}-{access_class_name}']
                    print("DEBUG: Access Reports: Class Set")
                    print(class_set)
                    if not class_set.empty:
                        class_set_yellow = class_set[class_set[self.get_gradezone_format(self.quarter, self.semester)] == 'Yellow']
                        class_set_red = class_set[class_set[self.get_gradezone_format(self.quarter, self.semester)] == 'Red']
                        if not class_set_yellow.empty or not class_set_red.empty:
                            ptext = 'Period ' + str(period) + ':'
                            table_story.append([ptext, '', '','','','',''])
                            table_story.append(['Student', 'ID', 'Class', 'Teacher', 'Period', 'Grade', 'Zone'])
                            for index, row in class_set_yellow.iterrows():
                                for class_period in PERIODS:
                                    if self.get_mark_format(class_period, self.semester, self.quarter) in row:
                                        if row[self.get_mark_format(class_period, self.semester, self.quarter)] == 'F':
                                            student_story = []
                                            student_story.append(row['Student Name'])
                                            student_story.append(str(row['Student ID']))
                                            student_story.append(row[self.get_course_format(class_period, self.semester)])
                                            student_story.append(row[self.get_teacher_format(class_period, self.semester)])
                                            student_story.append(str(class_period))
                                            student_story.append(row[self.get_mark_format(class_period, self.semester, self.quarter)])
                                            student_story.append("Yellow")
                                            table_story.append(student_story)
                            for index, row in class_set_red.iterrows():
                                for class_period in PERIODS:
                                    if self.get_mark_format(class_period, self.semester, self.quarter) in row:
                                        if row[self.get_mark_format(class_period, self.semester, self.quarter)] == 'F':
                                            student_story = []
                                            student_story.append(row['Student Name'])
                                            student_story.append(str(row['Student ID']))
                                            student_story.append(row[self.get_course_format(class_period, self.semester)])
                                            student_story.append(row[self.get_teacher_format(class_period, self.semester)])
                                            student_story.append(str(class_period))
                                            student_story.append(row[self.get_mark_format(class_period, self.semester, self.quarter)])
                                            student_story.append("Red")
                                            table_story.append(student_story)
            if table_story:
                table = Table(table_story)
                table.setStyle([('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                        ('BOX', (0,0), (-1,-1), 0.25, colors.black)])
                story.append(table)
            c.build(story)

            if password:
                document = PyPDF2.PdfFileReader('output/access_yellow_report/' + teacher_name + '.pdf')
                document_writer = PyPDF2.PdfFileWriter()
                for page in range(document.getNumPages()):
                    document_writer.addPage(document.getPage(page))
                document_writer.encrypt(user_pwd=password, owner_pwd=None, use_128bit=True)
                with open('output/access_yellow_report/' + teacher_name + '.pdf', 'wb') as file_handler:
                    document_writer.write(file_handler)
            message.tick_access_bar.emit()
        access_workbooks = None
        message.set_status.emit("Access Reports: Completed.")

    def load_staff(self):
        print("Loading staff")
        try:
            self.community_partners = set(self.full_data['Community Partner\nOrganization'].dropna().unique())
        except Exception as e:
            self.community_partners = set()
            print("Exception in generating community partners:", e)
        try:
            self.counselor_names = set(self.full_data['Counselor'].dropna().unique())
        except Exception as e:
            self.counselor_names = set()
            print("Exception in generating counselors:", e)
        message.set_status.emit("Sorting Access and AVID Teachers...")
        for index, row in self.full_data.iterrows():
            for period in PERIODS:
                if self.get_teacher_format(period, self.semester) in row:
                    if type(row[self.get_teacher_format(period, self.semester)]) is str:
                        self.teacher_names.add(row[self.get_teacher_format(period, self.semester)])
                        if type(row[self.get_course_format(period, self.semester)]) is str and '9th Grade Access' in row[self.get_course_format(period, self.semester)]:
                            self.access_teacher_names.add(row[self.get_teacher_format(period, self.semester)])
                        if type(row[self.get_course_format(period, self.semester)]) is str and 'AVID 9' in row[self.get_course_format(period, self.semester)]:
                            self.access_teacher_names.add(row[self.get_teacher_format(period, self.semester)])
                        if type(row[self.get_course_format(period, self.semester)]) is str and 'AVID 10' in row[self.get_course_format(period, self.semester)]:
                            self.access_teacher_names.add(row[self.get_teacher_format(period, self.semester)])
                        if type(row[self.get_course_format(period, self.semester)]) is str and 'AVID 11' in row[self.get_course_format(period, self.semester)]:
                            self.access_teacher_names.add(row[self.get_teacher_format(period, self.semester)])
                        if type(row[self.get_course_format(period, self.semester)]) is str and 'AVID 12' in row[self.get_course_format(period, self.semester)]:
                            self.access_teacher_names.add(row[self.get_teacher_format(period, self.semester)])
                        if type(row[self.get_course_format(period, self.semester)]) is str and 'Strategies for Success 9-10' in row[self.get_course_format(period, self.semester)]:
                            self.access_teacher_names.add(row[self.get_teacher_format(period, self.semester)])
                        if type(row[self.get_course_format(period, self.semester)]) is str and 'Academic Skills Tutorial' in row[self.get_course_format(period, self.semester)]:
                            self.access_teacher_names.add(row[self.get_teacher_format(period, self.semester)])
                        if type(row[self.get_course_format(period, self.semester)]) is str and 'ELD Access' in row[self.get_course_format(period, self.semester)]:
                            self.access_teacher_names.add(row[self.get_teacher_format(period, self.semester)])
        print("Debug: Community Partners:", self.community_partners)
        print("Debug: Counselor:", self.counselor_names)
        print("Debug: Access teachers:", self.access_teacher_names)
        print("Debug: Number of teachers:", len(self.teacher_names))


class ReportEngineV1N2023EarlyWarning(ReportEngineV1N2020):
    def __init__(self):
        self.full_data = None
        self.access_teacher_names = set([])
        self.teacher_names = set([])
        self.counselor_names = set([])
        self.community_partners = set([])
        self.workbook_stream = io.BytesIO()
        self.workbook_template_stream = io.BytesIO()
        self.headers = 0
    
    def get_mark_format(self, period, semester, quarter) -> str:
        return f"P{period} S{semester} Mark{quarter}"
    
    def get_course_format(self, period, semester) -> str:
        return f'P{period} S{semester} Course'
    
    def get_teacher_format(self, period, semester) -> str:
        return f'P{period} S{semester} Teacher'

    def get_gradezone_format(self, quarter, semester) -> str:
        return 'Marks Zone'

    def access_reports(self, password):
        # For 2022 edition books
        if not os.path.exists('output/access_yellow_report'):
            os.makedirs('output/access_yellow_report')
        message.set_teacher_bar_max.emit(int(len(self.teacher_names) * len(PERIODS) - 1))
        styles = getSampleStyleSheet()

        # F and Ds
        self.full_data['Marks Zone'] = ""
        for index in range(len(self.full_data)):
            student_track_counter = 0.0
            for period in PERIODS:
                if self.full_data.iloc[index][self.get_mark_format(period, self.semester, self.quarter)] == 'F':
                    student_track_counter += 1.0
                elif self.full_data.iloc[index][self.get_mark_format(period, self.semester, self.quarter)] == 'D':
                    student_track_counter += 0.5
            if student_track_counter >= 2.5:
                self.full_data.loc[index, self.get_gradezone_format(self.quarter, self.semester)] = 'Off-Track'
            elif student_track_counter > 1.0:
                self.full_data.loc[index, self.get_gradezone_format(self.quarter, self.semester)] = 'At-Risk'
            else:
                self.full_data.loc[index, self.get_gradezone_format(self.quarter, self.semester)] = 'On-Track'

        message.set_status.emit("Creating access pdf.")
        formatted_time = time.ctime()
        story = []
        c = SimpleDocTemplate('output/access_yellow_report/Da_Team.pdf', pagesize=letter)
        ptext = "CONFIDENTIAL: FERPA!"
        story.append(Paragraph(ptext, styles["Heading3"]))
        ptext = "Failing student report for Da Team. Generated on: " + str(formatted_time)
        story.append(Paragraph(ptext, styles["Normal"]))
        ptext = """I'm a bot. Beep Boop. The following students are in danger of failing life.
        Yellow zone students are passing 6-7 classes. Red zone students are passing 5 or less.
        Two Ds are treated like one F."""
        story.append(Paragraph(ptext, styles["Normal"]))
        table_story = []
        # At this point this is almost worth making a class
        class_set_red = self.full_data[self.full_data[self.get_gradezone_format(self.quarter, self.semester)] == 'Off-Track']
        class_set_yellow = self.full_data[self.full_data[self.get_gradezone_format(self.quarter, self.semester)] == 'At-Risk']
        table_story.append([ptext, 'Grade', 'Zone', 'Attendance'])
        for index in range(len(class_set_yellow)):
            ptext = class_set_yellow.iloc[index]['Name']
            grade_text = ""
            att_text = ""
            try:
                att_text = class_set_yellow.iloc[index]["YTD ATT RT"]
            except:
                print("Error with:", ptext)
            table_story.append([ptext, grade_text, "Yellow", att_text])
        for index in range(len(class_set_red)):
            ptext = class_set_red.iloc[index]['Name']
            grade_text = ""
            att_text = ""
            try:
                att_text = class_set_red.iloc[index]["YTD ATT RT"]
            except:
                print("Error with:", ptext)
            table_story.append([ptext, grade_text, "Red", att_text])        
            message.tick_teacher_bar.emit()
        if table_story:
            table = Table(table_story)
            table.setStyle([('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                    ('BOX', (0,0), (-1,-1), 0.25, colors.black)])
            story.append(table)
        c.build(story)
        
        document = PyPDF2.PdfReader('output/access_yellow_report/Da_Team.pdf')
        document_writer = PyPDF2.PdfWriter()
        for page in range(len(document.pages)):
            document_writer.add_page(document.pages[page])
        if password:
            document_writer.encrypt(user_pwd=password, owner_pwd=None, use_128bit=True)
        with open('output/access_yellow_report/Da_Team.pdf', 'wb') as file_handler:
            document_writer.write(file_handler)
        message.set_status.emit("Access report completed!")

    def teacher_reports(self, pdf=False, excel=False, password=None, email=False):
        if not os.path.exists('output/teacher_yellow_report'):
            os.makedirs('output/teacher_yellow_report')
        message.set_teacher_bar_max.emit(int(len(self.teacher_names) * len(PERIODS) - 1))
        styles = getSampleStyleSheet()

        # F and Ds
        if pdf:
            self.full_data['Marks Zone'] = ""
            for index in range(len(self.full_data)):
                student_track_counter = 0.0
                for period in PERIODS:
                    if self.full_data.iloc[index][self.get_mark_format(period, self.semester, self.quarter)] == 'F':
                        student_track_counter += 1.0
                    elif self.full_data.iloc[index][self.get_mark_format(period, self.semester, self.quarter)] == 'D':
                        student_track_counter += 0.5
                if student_track_counter >= 2.5:
                    self.full_data.loc[index, self.get_gradezone_format(self.quarter, self.semester)] = 'Off-Track'
                elif student_track_counter > 1.0:
                    self.full_data.loc[index, self.get_gradezone_format(self.quarter, self.semester)] = 'At-Risk'
                else:
                    self.full_data.loc[index, self.get_gradezone_format(self.quarter, self.semester)] = 'On-Track'

            for teacher_name in self.teacher_names:
                message.set_status.emit("Creating individual teacher pdf: " + teacher_name)
                formatted_time = time.ctime()
                story = []
                c = SimpleDocTemplate('output/teacher_yellow_report/' + teacher_name + '.pdf', pagesize=letter)
                ptext = "CONFIDENTIAL: FERPA!"
                story.append(Paragraph(ptext, styles["Heading3"]))
                ptext = "Failing student report for " + str(teacher_name) + ". Generated on: " + str(formatted_time)
                story.append(Paragraph(ptext, styles["Normal"]))
                ptext = """I'm a bot. Beep Boop. The following students are in danger of failing your class.
                Yellow zone students are passing 6-7 classes. Red zone students are passing 5 or less.
                Two Ds are treated like one F."""
                story.append(Paragraph(ptext, styles["Normal"]))
                table_story = []
                # At this point this is almost worth making a class
                for period in PERIODS:
                    if self.get_teacher_format(period, self.semester) in self.full_data:
                        class_set = self.full_data[self.full_data[self.get_teacher_format(period, self.semester)] == teacher_name]
                        if not class_set.empty:
                            class_set_red = class_set[class_set[self.get_gradezone_format(self.quarter, self.semester)] == 'Off-Track']
                            class_set_yellow = class_set[class_set[self.get_gradezone_format(self.quarter, self.semester)] == 'At-Risk']
                            class_set_green = class_set[class_set[self.get_gradezone_format(self.quarter, self.semester)] == 'On-Track']
                            green_inclass = class_set_green[class_set_green[self.get_mark_format(period, self.semester, self.quarter)] == 'D']
                            green_inclass = pd.concat([green_inclass, class_set_green[class_set_green[self.get_mark_format(period, self.semester, self.quarter)] == 'NP']])
                            green_inclass = pd.concat([green_inclass, class_set_green[class_set_green[self.get_mark_format(period, self.semester, self.quarter)] == 'F']])
                            green_students = list(green_inclass['Name'].values)
                            yellow_inclass = class_set_yellow[class_set_yellow[self.get_mark_format(period, self.semester, self.quarter)] == 'D']
                            yellow_inclass = pd.concat([yellow_inclass, class_set_yellow[class_set_yellow[self.get_mark_format(period, self.semester, self.quarter)] == 'NP']])
                            yellow_inclass = pd.concat([yellow_inclass, class_set_yellow[class_set_yellow[self.get_mark_format(period, self.semester, self.quarter)] == 'F']])
                            yellow_students = list(yellow_inclass['Name'].values)
                            red_inclass = class_set_red[class_set_red[self.get_mark_format(period, self.semester, self.quarter)] == 'D']
                            red_inclass = pd.concat([red_inclass, class_set_red[class_set_red[self.get_mark_format(period, self.semester, self.quarter)] == 'NP']])
                            red_inclass = pd.concat([red_inclass, class_set_red[class_set_red[self.get_mark_format(period, self.semester, self.quarter)] == 'F']])
                            red_students = list(red_inclass['Name'].values)
                            if yellow_students or red_students or green_students:
                                ptext = 'Period ' + str(period) + ':'
                                table_story.append([ptext, 'Grade', 'Zone', 'Attendance'])
                                for student in green_students:
                                    ptext = student
                                    grade_text = class_set_green.loc[class_set_green['Name'] == student][self.get_mark_format(period, self.semester, self.quarter)].values[0]
                                    att_text = ""
                                    try:
                                        att_text = class_set_green.loc[class_set_green['Name'] == student]["YTD ATT RT"].values[0]
                                    except:
                                        print("Error with:", student)
                                    table_story.append([ptext, grade_text, "Green", att_text])
                                for student in yellow_students:
                                    ptext = student
                                    grade_text = class_set_yellow.loc[class_set_yellow['Name'] == student][self.get_mark_format(period, self.semester, self.quarter)].values[0]
                                    att_text = ""
                                    try:
                                        att_text = class_set_yellow.loc[class_set_yellow['Name'] == student]["YTD ATT RT"].values[0]
                                    except:
                                        print("Error with:", student)
                                    table_story.append([ptext, grade_text, "Yellow", att_text])
                                for student in red_students:
                                    ptext = student
                                    grade_text = class_set_red.loc[class_set_red['Name'] == student][self.get_mark_format(period, self.semester, self.quarter)].values[0]
                                    att_text = ""
                                    try:
                                        att_text = class_set_red.loc[class_set_red['Name'] == student]["YTD ATT RT"].values[0]
                                    except:
                                        print("Error with:", student)
                                    table_story.append([ptext, grade_text, "Red", att_text])
                            print("Ran for:", teacher_name, "P", period)
                    table_story.append(['', '', '', ''])        
                    message.tick_teacher_bar.emit()
                if table_story:
                    table = Table(table_story)
                    table.setStyle([('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                            ('BOX', (0,0), (-1,-1), 0.25, colors.black)])
                    story.append(table)
                c.build(story)
                
                document = PyPDF2.PdfReader('output/teacher_yellow_report/' + teacher_name + '.pdf')
                document_writer = PyPDF2.PdfWriter()
                for page in range(len(document.pages)):
                    document_writer.add_page(document.pages[page])
                if password:
                    document_writer.encrypt(user_pwd=password, owner_pwd=None, use_128bit=True)
                with open('output/teacher_yellow_report/' + teacher_name + '.pdf', 'wb') as file_handler:
                    document_writer.write(file_handler)
                message.set_status.emit("Teacher reports completed!")


class ReportEngineV3(ReportEngineV1):
    '''Version 3 is a non SQL engine based on Report Engine V1. The major difference is that the output
    is based on slicing and dicing using Pandas instead of iterating and copying line by line.'''
    headers = {}

    # TODO: Create output template
    def load_sheet(self, sheet: str, semester=1, quarter=1):
        '''Load the specified sheet into a dataframe. Remove discipline data if present'''
        message.set_status.emit("Extracting excel spreadsheet.")
        message.set_main_bar_max.emit(4)
        self.sheet_name = sheet
        self.full_data = self.excel_pandas.parse(sheet_name=sheet, header=1)
        message.set_status.emit("Sheet extracted.")
        message.tick_main_bar.emit()
        self.semester = semester
        self.quarter = quarter
        message.tick_main_bar.emit()

        message.set_status.emit("Loading workbook. This step takes a while for large workbooks.")
        workbook = openpyxl.load_workbook(self.path)
        # Remove unsed sheets

        message.set_status.emit("Creating template.")
        message.tick_main_bar.emit()
        self.openpyxl_style = copy(workbook[self.sheet_name].conditional_formatting)
        print("Conditional formatting:", workbook[self.sheet_name].conditional_formatting)
        self.openpyxl_size = copy(workbook[self.sheet_name].column_dimensions)
        workbook[self.sheet_name].delete_rows(3, workbook[self.sheet_name].max_row)
        ## Test ##
        workbook.defined_names._cleanup()
        ## Test iter row on merged cell ##
        for row in workbook[self.sheet_name].iter_rows(min_row=1, max_row=1):
            for cell in row:
                if type(cell) == Cell:
                    value = copy(cell.value)
                    coordinate = copy(cell.coordinate)
                    column = cell.column
                    fill = copy(cell.fill)
                    font = copy(cell.font)
                    border = copy(cell.border)
                    alignment = copy(cell.alignment)
                    self.headers[coordinate] = {
                        'value': value,
                        'column': column,
                        'fill': fill,
                        'font': font,
                        'border': border,
                        'alignment': alignment
                    }
        ##      ##
        workbook.save(self.workbook_template_stream)
        if not os.path.exists('output'):
            os.makedirs('output')
        #workbook.save("output/template.xlsx")
        workbook.close()
        message.set_status.emit("Populating teacher lists.")
        message.tick_main_bar.emit()
        self.load_staff()
        message.set_status.emit("Spreadsheet extracted.")
        message.tick_main_bar.emit()
        message.super_finished_initial_extraction.emit()
    
    def counseling_reports(self):
        message.set_status.emit("Generating counseler reports...")
        path = 'output/counselor_report_test/'
        if not os.path.exists(path):
            os.makedirs(path)
        
        message.set_counselor_bar_max.emit(int(len(self.counselor_names)))
        message.tick_counselor_bar.emit()
        
        for counselor_name in self.counselor_names:
            message.set_status.emit("Creating individual counseler reports: " + counselor_name)
            counselor_dataframe = self.full_data[self.full_data['Counselor'] == counselor_name]
            counselor_dataframe.to_excel(path + str(counselor_name) + '.xlsx',
                                         sheet_name=counselor_name,
                                         index=False,
                                         freeze_panes=(2, 3))

            # Apply formatting
            # Reinsert missing headers
            wb = openpyxl.load_workbook(path + str(counselor_name) + '.xlsx')
            self.format_worksheet(wb[counselor_name])
            wb.save(path + str(counselor_name) + '.xlsx')


            message.set_status.emit("Saved individual counseler reports: " + counselor_name)
            message.tick_counselor_bar.emit()
        
        message.set_status.emit('Counselor report: DONE!')
    
    def format_worksheet(self, worksheet):
        headers = {}
        #worksheet.conditional_formatting = self.openpyxl_style
        worksheet.insert_rows(0)
        #headers =   {'A1': ('Student Info', 'FFFF00'),
        #            'K1': ('Counselor, Teacher, Class, Grades & Attendance Info', '66CC00'),
        #            'DQ1': ('Graduation Scorecard',),
        #            'DY1': ('Attendance',),
        #            'EM1': ('Discipline',),
        #            'ES1': ('Essential Skills - Reading',),
        #            'EX1': ('Essential Skills - Writing',),
        #            'FD1': ('Essential Skills - Math',),
        #            'FJ1': ("Success Zones (Green=0-1 F's, Yellow=2-3 F's, Red=4+ F's)",),
        #            'FZ1': ('Counselor Notes',)}
            
        worksheet.merge_cells('A1:J1')
        worksheet.merge_cells('K1:DP1')
        worksheet.merge_cells('DQ1:DX1')
        worksheet.merge_cells('DY1:EL1')
        worksheet.merge_cells('EM1:ER1')
        worksheet.merge_cells('ES1:EW1')
        worksheet.merge_cells('EX1:FC1')
        worksheet.merge_cells('FD1:FI1')
        worksheet.merge_cells('FJ1:FY1')
        worksheet.merge_cells('FZ1:GA1')

        for key, value in self.headers.items():
            worksheet[key] = value['value']
            worksheet[key].alignment = value['alignment']
            worksheet[key].fill = value['fill']
            worksheet[key].border = value['border']
        
        
        dim_holder = DimensionHolder(worksheet=worksheet)

        for col in range(worksheet.min_column, worksheet.max_column + 1):
            dim_holder[get_column_letter(col)] = ColumnDimension(worksheet, min=col, max=col, bestFit=True)

        #worksheet.column_dimensions = dim_holder
        worksheet.column_dimensions = self.openpyxl_size

        maxcolumnletter = get_column_letter(worksheet.max_column)
        worksheet.auto_filter.ref = 'A2:'+maxcolumnletter+str(len(worksheet['A']))

        # Conditional formatting
        # Gender
        dxf = DifferentialStyle(font=Font(color='002060'))
        rule = Rule(type="containsText", operator="containsText", dxf=dxf, text="M")
        #rule = Rule(type="cellIs", dxf=dxf, text="M")
        worksheet.conditional_formatting.add('D3:D' + str(worksheet.max_row), rule)
        dxf = DifferentialStyle(font=Font(color='993366'))
        rule = Rule(type="containsText", operator="containsText", dxf=dxf, text="F")
        #rule = Rule(type="cellIs", dxf=dxf, text="F")
        worksheet.conditional_formatting.add('D3:D' + str(worksheet.max_row), rule)